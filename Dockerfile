# base image
FROM node:12.2.0

# set working directory
WORKDIR /app
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# APK update
RUN apt-get update && \
    apt-get install -y git ssh gcc

# copy SSH keys in order to be able to fetch AMC dependencies from bitbucket
COPY ./keys/* /root/.ssh/

# install and cache app dependencies
COPY ./package.json /app
#RUN npm install --silent

# start app
CMD ["npm", "run", "start-container"]
