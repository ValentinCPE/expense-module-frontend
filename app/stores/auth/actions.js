/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  ENTRY_POINT,
  AUTH_FORBIDDEN,
  AUTH_CHECK_LOCAL_STORAGE,
  AUTHENTICATED,
  NOT_AUTHENTICATED,
} from './constants';

export function authForbidden() {
  return {
    type: AUTH_FORBIDDEN,
  };
}

// export function loadAuthByAccessToken(accessToken) {
//   return {
//     type: LOAD_AUTH_BY_ACCESS_TOKEN,
//     accessToken,
//   };
// }
export function authLocalStorage() {
  return {
    type: AUTH_CHECK_LOCAL_STORAGE.LOAD,
  };
}

export function authLocalStorageSuccess(accessToken, refreshToken) {
  return {
    type: AUTH_CHECK_LOCAL_STORAGE.SUCCESS,
    accessToken,
    refreshToken,
  };
}

export function authLocalStorageDenied() {
  return {
    type: AUTH_CHECK_LOCAL_STORAGE.DENIED,
  };
}

export function authLocalStorageError(error) {
  return {
    type: AUTH_CHECK_LOCAL_STORAGE.ERROR,
    error,
  };
}

export function entryPoint(accessToken) {
  return {
    type: ENTRY_POINT.LOAD,
    accessToken,
  };
}

export function entryPointSuccess(accessToken) {
  return {
    type: ENTRY_POINT.SUCCESS,
    accessToken,
  };
}

export function entryPointDenied() {
  return {
    type: ENTRY_POINT.DENIED,
  };
}

export function entryPointError() {
  return {
    type: ENTRY_POINT.ERROR,
  };
}
export function authenticated() {
  return {
    type: AUTHENTICATED,
  };
}
export function notAuthenticated() {
  return {
    type: NOT_AUTHENTICATED,
  };
}
