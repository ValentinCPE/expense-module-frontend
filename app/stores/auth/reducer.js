import { fromJS } from 'immutable';

import { AUTH } from 'api/expense/amc/constants';

import { TOKEN_CHANGE } from 'api/fiducialConnect/constants';
import {
  injectDefaultState,
  onLoad,
  onSuccess,
  onError,
} from 'helpers/reducer';
import {
  AUTH_FORBIDDEN,
  AUTH_CHECK_LOCAL_STORAGE,
  ENTRY_POINT,
  AUTHENTICATED,
  NOT_AUTHENTICATED,
} from './constants';

const initialState = fromJS({
  ...injectDefaultState,
  authenticated: false,
  accessToken: null,
  refreshToken: null,
  isAuthCheckingLocalStorage: false,
  isAuthCheckingEntryPoint: false,
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATED:
      return state.set('authenticated', true);
    case NOT_AUTHENTICATED:
      return state
        .set('authenticated', false)
        .set('accessToken', null)
        .set('refreshToken', null);
    // Logged by LocalStorage
    case AUTH_CHECK_LOCAL_STORAGE.LOAD:
      return onLoad(state).set('isAuthCheckingLocalStorage', true);
    case AUTH_CHECK_LOCAL_STORAGE.SUCCESS:
      return onSuccess(state)
        .set('authenticated', true)
        .set('accessToken', action.accessToken)
        .set('refreshToken', action.refreshToken)
        .set('isAuthCheckingLocalStorage', false);
    case AUTH_CHECK_LOCAL_STORAGE.DENIED:
      return onSuccess(state)
        .set('authenticated', false)
        .set('isAuthCheckingLocalStorage', false)
        .set('accessToken', null)
        .set('refreshToken', null);
    case AUTH_CHECK_LOCAL_STORAGE.ERROR:
      return onError(state, action)
        .set('authenticated', false)
        .set('isAuthCheckingLocalStorage', false)
        .set('accessToken', null)
        .set('refreshToken', null);
    // Logged by fiducial connect
    case ENTRY_POINT.LOAD:
      return onLoad(state).set('isAuthCheckingEntryPoint', true);
    case ENTRY_POINT.SUCCESS:
      return onSuccess(state)
        .set('authenticated', true)
        .set('accessToken', action.accessToken)
        .set('isAuthCheckingEntryPoint', false);
    case ENTRY_POINT.DENIED:
      return onSuccess(state)
        .set('authenticated', false)
        .set('isAuthCheckingEntryPoint', false);
    case ENTRY_POINT.ERROR:
      return onError(state, action)
        .set('authenticated', false)
        .set('isAuthCheckingEntryPoint', false)
        .set('accessToken', null);
    case TOKEN_CHANGE.SUCCESS:
      return state
        .set('accessToken', action.data.accessToken)
        .set('refreshToken', action.data.refreshToken);
    // Logged by login/pwd
    case AUTH.LOAD:
      return onLoad(state)
        .set('accessToken', null)
        .set('refreshToken', null);
    case AUTH.SUCCESS:
      return onSuccess(state)
        .set('authenticated', true)
        .set('accessToken', action.accessToken)
        .set('refreshToken', action.refreshToken);
    case AUTH.ERROR:
    case AUTH_FORBIDDEN:
      return onError(state, action)
        .set('authenticated', false)
        .set('accessToken', null)
        .set('refreshToken', null);
    default:
      return state;
  }
}

export default authReducer;
