import { call, put, takeLatest } from 'redux-saga/effects';
import { addSeconds, getTime } from 'date-fns';

import utility from 'api/expense/utility/saga';
import { AUTH as AUTH_TYPE } from 'api/expense/amc/constants';
import { authenticate } from 'api/expense/amc/saga';

import { DISABLE_LOCALE_STORAGE_TOKEN_IN } from 'config';

import {
  authLocalStorageSuccess,
  authLocalStorageDenied,
  authLocalStorageError,
  authForbidden,
  entryPointSuccess,
  entryPointDenied,
  entryPointError,
} from './actions';
import { AUTH_CHECK_LOCAL_STORAGE, ENTRY_POINT } from './constants';

const LOCAL_STORAGE = {
  ACCESS_TOKEN: '__access_token',
  TOKEN_EXPIRE: '__token_expiration',
};

// TODO: To change (middleware?)
export const AUTH = {
  ACCESS_TOKEN: null,
};

function getAuthToken() {
  return JSON.parse(localStorage.getItem(LOCAL_STORAGE.ACCESS_TOKEN));
}

function* setAuthToken(token) {
  AUTH.ACCESS_TOKEN = token;
  localStorage.setItem(LOCAL_STORAGE.ACCESS_TOKEN, JSON.stringify(token));
  localStorage.setItem(
    LOCAL_STORAGE.TOKEN_EXPIRE,
    getTime(addSeconds(new Date(), DISABLE_LOCALE_STORAGE_TOKEN_IN)),
  );
  yield token;
}

function removeAuthToken() {
  AUTH.ACCESS_TOKEN = null;
  localStorage.removeItem(LOCAL_STORAGE.ACCESS_TOKEN);
}
function removeTokenExpiration() {
  localStorage.removeItem(LOCAL_STORAGE.TOKEN_EXPIRE);
}

function removeAuthInformation() {
  removeAuthToken();
  removeTokenExpiration();
}

export function* handleXHRError(error, cb) {
  if (error.response && error.response.status === 403) {
    removeAuthInformation();
    yield put(authForbidden());
  }
  yield put(cb(error));
}

export function* authLocalStorage() {
  try {
    const localStorageToken = getAuthToken();
    if (localStorageToken != null) {
      try {
        yield call(setAuthToken, localStorageToken); // Ping need the token
        const { error } = yield call(utility.ping);
        if (!error) {
          yield call(setAuthToken, localStorageToken);
          yield put(authLocalStorageSuccess(localStorageToken));
          return;
        }
        /* eslint-disable no-empty */
      } catch (err) {}
    }
    removeAuthInformation();
    yield put(authLocalStorageDenied());
  } catch (err) {
    removeAuthInformation();
    yield call(handleXHRError, err, authLocalStorageError);
  }
}

export function* entryPoint({ accessToken }) {
  try {
    if (accessToken != null) {
      try {
        yield call(setAuthToken, accessToken); // Ping need the token
        const { error } = yield call(utility.ping);
        if (!error) {
          yield call(setAuthToken, accessToken);
          yield put(entryPointSuccess(accessToken));
          return;
        }
        /* eslint-disable no-empty */
      } catch (err) {}
    }
    removeAuthInformation();
    yield put(entryPointDenied());
  } catch (err) {
    removeAuthInformation();
    yield call(handleXHRError, err, entryPointError);
  }
}

function* authenticateSuccess({ accessToken }) {
  yield call(setAuthToken, accessToken);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* authData() {
  yield [
    takeLatest(ENTRY_POINT.LOAD, entryPoint),
    takeLatest(ENTRY_POINT.SUCCESS, authenticateSuccess),
    takeLatest(AUTH_TYPE.LOAD, authenticate),
    takeLatest(AUTH_TYPE.SUCCESS, authenticateSuccess),
    takeLatest(AUTH_CHECK_LOCAL_STORAGE.LOAD, authLocalStorage),
  ];
}
