// import { toJS } from 'helpers/selectors';

import { createSelector } from 'reselect';
import { NAMESPACE } from './constants';

const selectAuth = state => state.get(NAMESPACE);

const makeSelectLoading = () =>
  createSelector(
    selectAuth,
    authState => authState.get('loading'),
  );

const makeSelectError = () =>
  createSelector(
    selectAuth,
    authState => authState.get('error'),
  );

const getAccessToken = () =>
  createSelector(
    selectAuth,
    authState => authState.get('accessToken'),
  );

const getRefreshToken = () =>
  createSelector(
    selectAuth,
    authState => authState.get('refreshToken'),
  );

const isAuthenticated = () =>
  createSelector(
    selectAuth,
    authState => authState.get('authenticated'),
  );

const isAuthCheckingLocalStorage = () =>
  createSelector(
    selectAuth,
    authState => authState.get('isAuthCheckingLocalStorage'),
  );

const isAuthCheckingEntryPoint = () =>
  createSelector(
    selectAuth,
    authState => authState.get('isAuthCheckingEntryPoint'),
  );

export {
  selectAuth,
  makeSelectLoading,
  makeSelectError,
  getAccessToken,
  getRefreshToken,
  isAuthenticated,
  isAuthCheckingLocalStorage,
  isAuthCheckingEntryPoint,
};
