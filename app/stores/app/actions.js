/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  UPDATE_CURRENT_CUSTOMER,
  LOAD_CURRENT_PERMISSION,
  LOAD_CURRENT_PERMISSION_SUCCESS,
  LOAD_CURRENT_PERMISSION_ERROR,
  INIT,
  RESET_APP,
} from './constants';

export function updateCurrentCustomer(customerId, customer) {
  return {
    type: UPDATE_CURRENT_CUSTOMER,
    customer,
  };
}

export function loadPermission() {
  return {
    type: LOAD_CURRENT_PERMISSION,
  };
}

export function permissionLoaded(permission) {
  return {
    type: LOAD_CURRENT_PERMISSION_SUCCESS,
    permission,
  };
}

export function permissionLoadingError(error) {
  return {
    type: LOAD_CURRENT_PERMISSION_ERROR,
    error,
  };
}

export function init() {
  return {
    type: INIT.LOAD,
  };
}

export function initSuccess() {
  return {
    type: INIT.SUCCESS,
  };
}

export function initError() {
  return {
    type: INIT.ERROR,
  };
}

export function resetApp() {
  return {
    type: RESET_APP,
  };
}
