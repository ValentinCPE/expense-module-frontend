import { fromJS } from 'immutable';

import { GET_PERMISSION, GET_PROFILE } from 'api/expense/amc/constants';

import {
  injectDefaultState,
  onLoad,
  onSuccess,
  onError,
} from 'helpers/reducer';

import { INIT, UPDATE_CURRENT_CUSTOMER } from './constants';

// The initial state of the App
export const initialState = fromJS({
  ...injectDefaultState,
  // Current
  currentUser: null,
  ready: false,
  currentPermission: {},
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case INIT.LOAD:
      return state.set('ready', false);
    case INIT.SUCCESS:
      return state.set('ready', true);
    case UPDATE_CURRENT_CUSTOMER:
      return state.set('currentCustomer', fromJS(action.customer));
    case GET_PERMISSION.LOAD:
      return onLoad(state).set('currentPermission', {});
    case GET_PERMISSION.SUCCESS:
      return onSuccess(state).set(
        'currentPermission',
        fromJS(action.resp.data.permission),
      );
    case GET_PROFILE.LOAD:
      return onLoad(state).set('isProfileLoading', true);
    case GET_PROFILE.SUCCESS:
      return onSuccess(state)
        .set('isProfileLoading', false)
        .set('currentUser', fromJS(action.resp.data || {}));
    case GET_PERMISSION.ERROR:
    case GET_PROFILE.ERROR:
      return onError(state, action);
    default:
      return state;
  }
}

export default appReducer;
