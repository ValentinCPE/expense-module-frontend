// import request from 'utils/request';
import { call, put, takeLatest, all } from 'redux-saga/effects';

import { getPermission, getProfile } from 'api/expense/amc/saga';
import { GET_PERMISSION, GET_PROFILE } from 'api/expense/amc/constants';

import { INIT } from './constants';
import { initSuccess, initError } from './actions';

export function* prefetchOnLogin() {
  try {
    yield all([call(getPermission), call(getProfile)]);
    yield put(initSuccess());
  } catch (err) {
    yield call(initError, err);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* appData() {
  yield takeLatest(GET_PERMISSION.LOAD, getPermission);
  yield takeLatest(GET_PROFILE.LOAD, getProfile);
  yield takeLatest(INIT.LOAD, prefetchOnLogin);
}
