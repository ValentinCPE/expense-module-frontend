import reducer from './reducer';
import * as selectors from './selectors';
import { NAMESPACE } from './constants';

export const REDUCER = { key: NAMESPACE, reducer };
export const SELECTORS = selectors;

export default {
  REDUCER,
  SELECTORS,
};
