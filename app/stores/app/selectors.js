import { toJS } from 'helpers/selectors';

import { createSelector } from 'reselect';
import { NAMESPACE } from './constants';
import { initialState } from './reducer';

const selectApp = state => state.get(NAMESPACE, initialState);

// const selectRoute = (state) => state.get('route');

const makeSelectCurrentUser = () =>
  createSelector(
    selectApp,
    appState => toJS(appState.get('currentUser')),
  );

const makeSelectLoading = () =>
  createSelector(
    selectApp,
    appState => appState.get('loading'),
  );

const makeSelectError = () =>
  createSelector(
    selectApp,
    appState => appState.get('error'),
  );

const makeSelectCurrentCustomer = () =>
  createSelector(
    selectApp,
    appState => toJS(appState.get('currentCustomer')),
  );

const makeSelectCurrentPermission = () =>
  createSelector(
    selectApp,
    appState => toJS(appState.get('currentPermission')),
  );

const hasFetchedUserProfile = () =>
  createSelector(
    selectApp,
    globalSelect => globalSelect.get('hasFetchedUserProfile'),
  );

const isReady = () =>
  createSelector(
    selectApp,
    globalSelect => globalSelect.get('ready'),
  );

// const makeSelectRepos = () =>
//   createSelector(selectApp, (appState) =>
//     appState.getIn(['userData', 'repositories'])
//   );

// const makeSelectLocation = () =>
//   createSelector(selectRoute, (routeState) => routeState.get('location').toJS());

export {
  selectApp,
  makeSelectCurrentCustomer,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  hasFetchedUserProfile,
  isReady,
  makeSelectCurrentPermission,
};
