import Dashboard from 'containers/Dashboard/Loadable';
import AdminDashboard from 'containers/AdminDashboard/Loadable';
import { ROLES } from './permission';
import { Entities } from './model/Entities';
import ListEntityLoaderComponent from './containers/Entity/List';
import DetailEntityLoaderComponent from './containers/Entity/Detail';

// const IMPORTED_EXTRA_PERMISSION = { ...EXTRA_PERMISSION };
const entitiesList = [];
const entitiesDetail = [];

export const LOGIN = {
  path: '/login',
};

export const DASHBOARD = {
  title: 'Dashboard',
  path: '/app/dashboard',
  icon: 'dashboard',
  component: Dashboard,
};

function getEntitiesListRoutes() {
  if (entitiesList.length === 0) {
    Object.values(Entities).forEach(entity => {
      entitiesList.push({
        type: entity,
        title: entity.label,
        path: `/app/${entity.nameUrl}/list`,
        icon: entity.iconMenu,
        component: ListEntityLoaderComponent,
        isExact: true,
      });
    });
  }
  return entitiesList;
}

function getEntitiesDetailRoutes() {
  if (entitiesDetail.length === 0) {
    Object.values(Entities).forEach(entity => {
      entitiesDetail.push({
        type: entity,
        title: entity.label,
        path: `/app/${entity.nameUrl}/:id`,
        icon: entity.iconMenu,
        component: DetailEntityLoaderComponent,
        isExact: true,
      });
    });
  }
  return entitiesDetail;
}

export const ADMIN_DASHBOARD = {
  title: 'Dashboard',
  path: '/admin/dashboard',
  icon: 'dashboard',
  component: AdminDashboard,
};

export const ADMIN_MENU = {
  title: 'Admin',
  allowed: [ROLES.ADMIN],
  path: ADMIN_DASHBOARD.path,
  key: 'admin_menu',
  icon: 'rocket',
};

/**
 * {
 *    path: string, // PATH URI
 *    allowed: array, // If one permission is matching, show
 *    // -- Menu
 *    title: string, // Node
 *    icon: string, // <Icon type /> to apply
 *    subTree: array, // SubMenu Tree
 *    // -- Route
 *    component: component // Setup route
 * }
 */
export const MENU_TREE = [
  DASHBOARD,
  {
    ...ADMIN_MENU,
    subTree: [ADMIN_DASHBOARD],
  },
];

export const getMenuTree = () => MENU_TREE.concat(getEntitiesListRoutes());

export const ROUTES = [DASHBOARD, ADMIN_DASHBOARD];

export const getRoutes = () =>
  ROUTES.concat(getEntitiesListRoutes(), getEntitiesDetailRoutes());

export const HOMEPAGE_ORDERED_ROLE = [
  {
    role: ROLES.ADMIN,
    page: ADMIN_DASHBOARD,
  },
];

export const PATH = {
  LOGIN: LOGIN.path,
  DASHBOARD: DASHBOARD.path,
  // RFELIST: RFELIST.path,
  // RFEDETAIL: RFEDETAIL.path,
  // INVOICE: INVOICE.path,
  // RFR: RFR.path,
  ADMIN_DASHBOARD: ADMIN_DASHBOARD.path,
  DEFAULT_ON_LOGIN: DASHBOARD.path, // Auto-Redirect when logged-in
  FC_ENTRY_POINT: '/entrypoint',
};
