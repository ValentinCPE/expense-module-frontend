import _ from 'lodash';

/**
 *
 * @param {array<string>|string} from
 * @param {array<string>} permissions
 */
export function hasPermission(from, permissions) {
  if (_.isString(from)) {
    return permissions[from];
  }
  return !from.every(permissionName => !permissions[permissionName]);
}
