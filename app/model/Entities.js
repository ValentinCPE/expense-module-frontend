export const Entities = Object.freeze({
  RFE: {
    key: 'RFE',
    label: 'Request For Expense',
    nameUrl: 'request-for-expense',
    iconMenu: 'form',
  },
  INVOICE: {
    key: 'INVOICE',
    label: 'Invoice',
    nameUrl: 'invoice',
    iconMenu: 'form',
  },
  RFR: {
    key: 'RFR',
    label: 'Request For Reimbursement',
    nameUrl: 'request-for-reimbursement',
    iconMenu: 'form',
  },
});
