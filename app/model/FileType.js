export const FileTypeKnown = Object.freeze({
  PDF: {
    extensionDisplayed: 'pdf',
    icon: 'file-pdf',
  },
  JPG: {
    extensionDisplayed: 'jpg',
    icon: 'file-image',
  },
  PNG: {
    extensionDisplayed: 'png',
    icon: 'file-image',
  },
  ZIP: {
    extensionDisplayed: 'zip',
    icon: 'file-zip',
  },
  WORD: {
    extensionDisplayed: 'docx',
    icon: 'file-word',
  },
  EXCEL: {
    extensionDisplayed: 'xlsx',
    icon: 'file-excel',
  },
});

export const FileTypeUnknown = Object.freeze({
  extensionDisplayed: '',
  icon: 'file',
});
