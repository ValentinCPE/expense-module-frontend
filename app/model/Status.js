// Objects freeze : https://stackoverflow.com/a/33128023
export const Status = Object.freeze({
  DRAFT: {
    label: 'DRAFT',
    textDisplayed: 'To be completed',
    color: 'volcano',
  },
  SUBMIT: {
    label: 'SUBMIT',
    textDisplayed: 'Submitted',
    color: 'volcano',
  },
  IN_APPROBATION: {
    label: 'IN APPROBATION',
    textDisplayed: 'Waiting for review',
    color: 'volcano',
  },
  ACCEPTED: {
    label: 'ACCEPTED',
    textDisplayed: 'Request accepted',
    color: 'green',
  },
  REJECTED: {
    label: 'REJECTED',
    textDisplayed: 'Request rejected',
    color: 'red',
  },
});
