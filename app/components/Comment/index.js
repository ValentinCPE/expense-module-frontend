import React from 'react';
import PropTypes from 'prop-types';
import {
  Timeline,
  TimelineAction,
  TimelineItem,
} from 'fiducial-react-components/es';

export const Comment = ({ comments }) => (
  <div>
    <Timeline
      dataSource={comments}
      renderItem={({ id, creationDate, text, username, logo, attachments }) => {
        const actions = [];
        if (attachments && attachments.length) {
          actions.unshift(
            <TimelineAction
              icon="paper-clip"
              text={`Attachments (${attachments.length})`}
              title="View Attachments"
            />,
          );
        }
        return (
          <TimelineItem
            key={id}
            actions={actions}
            avatarIcon={logo}
            avatarText={creationDate.toLocaleDateString()}
            title={username}
            description={text}
          />
        );
      }}
    />
  </div>
);

Comment.defaultProps = {
  comments: [
    {
      creationDate: new Date(),
      text:
        'Lorem ipsum, It is a great test. we like it so much. Thanks React.',
      username: 'Valentin',
    },
  ],
};

Comment.propTypes = {
  comments: PropTypes.array,
};

export default Comment;
