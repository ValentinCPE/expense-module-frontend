import React from 'react';
import PropTypes from 'prop-types';
import { Col, Popover, Row, Tag } from 'antd';
import styles from './style.less';
import { Currency } from '../../Format';
import { getWorkflowSteps } from '../../../helpers/workflow';
import { Status } from '../../../model/Status';

export const InfoEntity = ({ entity }) => (
  <section className={styles.infoDetailEntity}>
    <Row key="row1Info">
      <Col offset={3} span={2}>
        <span>Fiducial entity :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>{entity.fiducialEntity}</span>
      </Col>
      <Col offset={7} span={3}>
        <span>Budget allowance :</span>
      </Col>
      <Col offset={0} span={2}>
        <span className={styles.valueInfo}>
          <Currency value={entity.budgetAllowance} />
        </span>
      </Col>
    </Row>
    <Row key="row2Info" className={styles.spaceBetweenRowsInfo}>
      <Col offset={3} span={2}>
        <span>Budget code :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>{entity.budgetCode}</span>
      </Col>
      <Col offset={7} span={3}>
        <span>Frequency :</span>
      </Col>
      <Col offset={0} span={2}>
        <span className={styles.valueInfo}>{entity.frequency}</span>
      </Col>
    </Row>
    <Row key="row3Info" className={styles.spaceBetweenRowsInfo}>
      <Col offset={3} span={2}>
        <span>G/L account :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>{entity.glAccount}</span>
      </Col>
      <Col offset={7} span={3}>
        <span>Payment method :</span>
      </Col>
      <Col offset={0} span={2}>
        <span className={styles.valueInfo}>{entity.paymentMethod}</span>
      </Col>
    </Row>
    <Row key="row4Info" className={styles.spaceBetweenRowsInfo}>
      <Col offset={3} span={2}>
        <span>Vendor(s) :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>{entity.vendors.join(', ')}</span>
      </Col>
      <Col offset={7} span={3}>
        <span>Due date :</span>
      </Col>
      <Col offset={0} span={2}>
        <span className={styles.valueInfo}>
          {entity.dueDate.toLocaleDateString()}
        </span>
      </Col>
    </Row>
    <Row key="row5Info" className={styles.spaceBetweenRowsInfo}>
      <Col offset={3} span={2}>
        <span>Creation date :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>
          {entity.creationDate.toLocaleDateString()}
        </span>
      </Col>
      <Col offset={7} span={3}>
        <span>Payer :</span>
      </Col>
      <Col offset={0} span={2}>
        <span className={styles.valueInfo}>{entity.payer}</span>
      </Col>
    </Row>
    <Row key="row6Info" className={styles.spaceBetweenRowsInfo}>
      <Col offset={3} span={2}>
        <span>Creator :</span>
      </Col>
      <Col offset={1} span={2}>
        <span className={styles.valueInfo}>{entity.creator}</span>
      </Col>
      <Col offset={7} span={3}>
        <span>Status :</span>
      </Col>
      <Col offset={0} span={2}>
        <span>
          <Popover content={getWorkflowSteps(Status[entity.status], 'small')}>
            <Tag
              color={Status[entity.status].color}
              key={Status[entity.status]}
            >
              {Status[entity.status].label.toUpperCase()}
            </Tag>
          </Popover>
        </span>
      </Col>
    </Row>
  </section>
);

InfoEntity.defaultProps = {
  entity: {
    fiducialEntity: 'N/A',
    budgetAllowance: 0,
    budgetCode: 'N/A',
    frequency: 'N/A',
    glAccount: 'N/A',
    paymentMethod: 'N/A',
    vendors: ['N/A'],
    dueDate: new Date(),
    creationDate: new Date(),
    payer: 'N/A',
    creator: 'N/A',
  },
};

InfoEntity.propTypes = {
  entity: PropTypes.object,
};
