import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Col, Empty, Icon, Row, Tooltip } from 'antd';
import styles from './style.less';
import { getDateFullString } from '../../helpers/date';
import { InfoEntity } from './infoEntity';
import { getFileTypeAccordingFormat } from '../../helpers/fileAction';
import Comment from '../Comment';
import { PopupEntity } from '../PopupEntity';
import { Currency } from '../Format';

let autoApprovalEnabled;
let isAcceptPopup = false;

class EntityDetail extends React.Component {
  state = {
    loadingButtonConfirm: false,
    visiblePopup: false,
    textOkButton: getTextButton(false),
  };

  clickPlusAttachments = () => {};

  getFiles = () => {
    const files = [];
    let sizeOneElt = 24;
    if (
      !this.props.entity.attachments ||
      this.props.entity.attachments.length === 0
    ) {
      sizeOneElt = 12;
      files.push(
        <Col key="no-file" span={sizeOneElt}>
          <Empty description="No attachment" />
        </Col>,
      );
    } else {
      let keyIndex = 0;
      sizeOneElt = 24 / (this.props.entity.attachments.length + 1); // Size column for one element (+1 is for plus icon)
      this.props.entity.attachments.forEach((attach, index) => {
        keyIndex = index; // Key index cause lint does not accept index forEach as a key
        const fileType = getFileTypeAccordingFormat(attach.type);
        files.push(
          <Col
            span={sizeOneElt}
            key={`file${keyIndex}`}
            className={[
              styles.attachmentsContainer,
              styles.buttonClickable,
            ].join(' ')}
          >
            <Icon
              type={fileType.icon}
              className={styles.iconButtons}
              style={{ fontSize: '2.3em' }}
            />
            <section>{`${attach.name}.${fileType.extensionDisplayed}`}</section>
          </Col>,
        );
      });
    }
    files.push(
      <Col
        span={sizeOneElt}
        key="icon-sendAttachment"
        className={styles.buttonPlusAttachment}
      >
        <Tooltip title="Send an attachment">
          <Icon
            type="plus-circle"
            className={[styles.iconButtons, styles.buttonClickable]}
            theme="twoTone"
            onClick={this.clickPlusAttachments}
          />
        </Tooltip>
      </Col>,
    );
    return files;
  };

  launchPopup = () => {
    this.setState({
      visiblePopup: true,
    });
  };

  onOkCallback = () => {
    this.setState({});
  };

  onCancelCallback = () => {
    this.setState({
      visiblePopup: false,
    });
  };

  onChangeAutoApprovalBox = value => {
    if (value !== autoApprovalEnabled) {
      this.setState({
        textOkButton: getTextButton(value),
      });
    }
    autoApprovalEnabled = value;
  };

  setPopupAcceptEntity = value => {
    isAcceptPopup = value;
    this.launchPopup();
  };

  render() {
    const { entityType, entity } = this.props;
    const { visiblePopup, loadingButtonConfirm, textOkButton } = this.state;
    const propsPopup = {
      entityType,
      entity,
      title: 'Confirm your decision',
      visible: visiblePopup,
      loadingButton: loadingButtonConfirm,
      onOkText: textOkButton,
      onOkCallback: this.onOkCallback,
      onCancelText: 'Deny',
      onCancelCallback: this.onCancelCallback,
      isAcceptPopup,
      onChangeAutoApprovalBox: this.onChangeAutoApprovalBox,
    };

    return (
      <div>
        <section>
          <Row key="title">
            <Col offset={1} span={6}>
              <span
                className={styles.title}
              >{`${entityType.key} #${entity.id}`}</span>
            </Col>
            <Col offset={10} span={6}>
              <div className={styles.lastUpdatePosition}>
                <span>LAST UPDATE :</span>
                <span className={styles.labelLastUpdate}>
                  {entity.updatedDate.toLocaleDateString(
                    'en-US',
                    getDateFullString(),
                  )}
                </span>
              </div>
            </Col>
          </Row>
          <Row key="row-entity-info">
            <Col offset={2} span={20}>
              <Card
                title="Details"
                className={styles.spaceBetweenRowsInfo}
                extra={
                  <div className={styles.buttonsDetailsCard}>
                    <Button
                      type="primary"
                      size="small"
                      className={styles.spaceBeetweenButtonsDetailCard}
                    >
                      <Icon type="container" className={styles.iconButtons} />
                      Archive
                    </Button>
                    <Button
                      type="primary"
                      size="small"
                      className={styles.spaceBeetweenButtonsDetailCard}
                    >
                      <Icon type="edit" className={styles.iconButtons} />
                      Edit
                    </Button>
                  </div>
                }
              >
                <InfoEntity entity={entity} />
              </Card>
            </Col>
          </Row>
          <Row
            key="cards-attachement-chain"
            className={styles.spaceBetweenRowsInfo}
          >
            <Col offset={3} span={5}>
              <Card title="Attachments">
                <Row key="files-attachments">{this.getFiles()}</Row>
              </Card>
            </Col>
            <Col offset={6} span={8}>
              <Card title="Chain of approbation">
                <Row key="chain-approbation">TO DO ! Chain of approbation</Row>
              </Card>
            </Col>
          </Row>
          <Row
            key="card-general-action"
            className={styles.spaceBetweenRowsInfo}
          >
            <Col offset={7} span={8}>
              <Card title="Actions">
                <Row key="row-amount-action">
                  <Col offset={0} span={24}>
                    <div className={styles.amountDiv}>
                      Amount :{' '}
                      <span className={styles.amountLabel}>
                        <Currency value={entity.amount} />
                      </span>
                    </div>
                  </Col>
                </Row>
                <Row
                  key="row-button-action"
                  className={styles.spaceBetweenAmountButton}
                >
                  <Col offset={2} span={7}>
                    <Button
                      onClick={() => this.setPopupAcceptEntity(false)}
                      size="large"
                      style={{
                        width: '100%',
                        backgroundColor: '#ff4d4f',
                        color: 'white',
                        borderColor: 'white',
                      }}
                    >
                      <span className={styles.labelButtonAction}>REJECT</span>
                    </Button>
                  </Col>
                  <Col offset={6} span={7}>
                    <Button
                      onClick={() => this.setPopupAcceptEntity(true)}
                      size="large"
                      style={{
                        width: '100%',
                        backgroundColor: '#70C040',
                        color: 'white',
                        borderColor: 'white',
                      }}
                    >
                      <span className={styles.labelButtonAction}>ACCEPT</span>
                    </Button>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
          <Row key="row-comment" className={styles.spaceBetweenRowsInfo}>
            <Col offset={2} span={20}>
              <Card title="Comments">
                <Comment comments={entity.comments} />
              </Card>
            </Col>
          </Row>
        </section>
        <PopupEntity {...propsPopup} />
      </div>
    );
  }
}

function getTextButton(isAutoApproval) {
  return isAutoApproval ? 'Confirm\nand\nenable auto-approval' : 'Confirm';
}

EntityDetail.propTypes = {
  entityType: PropTypes.object,
  entity: PropTypes.object,
};

export default EntityDetail;
