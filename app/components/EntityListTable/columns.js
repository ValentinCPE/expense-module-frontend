import React from 'react';
import { Col, Divider, Icon, Popover, Row, Tag, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import { Currency } from '../Format';
import { Status } from '../../model/Status';
import { getWorkflowSteps } from '../../helpers/workflow';
import { getDateFullString } from '../../helpers/date';

import styles from './style.less';

function contentPopoverStatus(record) {
  const tag = [];
  if (record.needApproval) {
    tag.push(
      <Row key={`status${tag.length}`}>
        <Col span={6} offset={8}>
          <Tag color="red" key={record.id}>
            NEED MY APPROVAL
          </Tag>
        </Col>
      </Row>,
    );
  }

  return (
    <div>
      <section className={styles.approbationCircuit}>TBD</section>
      <section className={styles.centerNeedApprovalLabel}>{tag}</section>
      <Divider
        className={styles.needApprovalOnDividerPopOver}
        type="horizontal"
      />
      {getWorkflowSteps(Status[record.label], 'small')}
    </div>
  );
}

export const getColumns = propsComp => [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
    width: '12%',
    render: id => (
      <Tooltip title={`${propsComp.entity.key} #${id}`}>
        <Link to={`/app/${propsComp.entity.nameUrl}/${id}`}>{id}</Link>
      </Tooltip>
    ),
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    sorter: (a, b) => b.amount - a.amount,
    render: amount => <Currency value={amount} />,
  },
  {
    title: 'Creation date',
    dataIndex: 'creationDate',
    key: 'creationDate',
    sorter: (a, b) => b.creationDate - a.creationDate,
    render: date => <span>{date.toLocaleDateString()}</span>,
  },
  {
    title: 'Updated date',
    dataIndex: 'updatedDate',
    key: 'updatedDate',
    sorter: (a, b) => b.updatedDate - a.updatedDate,
    render: date => {
      const dateStringDetailed = date.toLocaleDateString(
        'en-US',
        getDateFullString(),
      );
      return (
        <Tooltip title={dateStringDetailed}>
          <span>{date.toLocaleDateString()}</span>
        </Tooltip>
      );
    },
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    render: (label, record) => (
      <span>
        <Popover content={contentPopoverStatus(record)}>
          <Tag color={Status[label].color} key={label}>
            {label.toUpperCase()}
          </Tag>
        </Popover>
      </span>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: value => (
      <span>
        <Tooltip title="View detail">
          <Link to={`/app/${propsComp.entity.nameUrl}/${value.id}`}>
            <Icon className={styles.clickable} type="search" />
          </Link>
        </Tooltip>
        <Divider type="vertical" />
        <Tooltip title={`Archive ${propsComp.entity.key}`}>
          <Icon className={styles.clickable} type="container" />
        </Tooltip>
      </span>
    ),
  },
];
