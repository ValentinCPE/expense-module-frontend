import React from 'react';
import {
  Button,
  Cascader,
  Col,
  DatePicker,
  Icon,
  Input,
  Row,
  Switch,
  Table,
} from 'antd';
import PropTypes from 'prop-types';
import { Status } from '../../model/Status';
import {
  searchFilters,
  setDateBeginningFilterAttribute,
  setDateEndingFilterAttribute,
  setIdFilterAttribute,
  setNeedApprovalFilterAttribute,
  setOnlyMyEntitiesFilterAttribute,
  setStatusFilterAttribute,
} from '../../helpers/filter';
import { getDateFullString } from '../../helpers/date';

import styles from './style.less';
import { getLastUpdatedEntityFromArray } from '../../helpers/entityAction';
import { getColumns } from './columns';

const { RangePicker } = DatePicker;

const dateFormat = 'MM/DD/YYYY';

let data = [];

class EntityListTable extends React.Component {
  state = {
    dataDisplayed: [],
    lastUpdatedEntity: null,
    optionsStatusCascader: [],
    selectedRowKeys: [],
  };

  componentWillReceiveProps(nextProps) {
    if (
      !nextProps.data ||
      nextProps.data.length === 0 ||
      !nextProps.onlyMyData ||
      nextProps.onlyMyData.length === 0
    ) {
      throw new Error('Parameters data and onlyMyData undefined or empty');
    }

    [...data] = nextProps.data;

    this.setState({
      dataDisplayed: data,
      lastUpdatedEntity: getLastUpdatedEntityFromArray(nextProps.data),
      optionsStatusCascader: this.fillCascader(),
    });
  }

  fillCascader = () => {
    const statusCascader = [];
    Object.keys(Status).forEach(item => {
      statusCascader.push({
        value: Status[item].label,
        label: Status[item].label,
      });
    });
    return statusCascader;
  };

  editIDFilter = idInput => {
    setIdFilterAttribute(idInput.target.value);
    this.applyFilters();
  };

  editDateFilter = dateInput => {
    if (dateInput[0] && dateInput[1]) {
      setDateBeginningFilterAttribute(dateInput[0].toDate());
      setDateEndingFilterAttribute(dateInput[1].toDate());
    } else {
      setDateBeginningFilterAttribute(null);
      setDateEndingFilterAttribute(null);
    }
    this.applyFilters();
  };

  editStatusFilter = status => {
    setStatusFilterAttribute(status[0]);
    this.applyFilters();
  };

  switchOnlyMine = value => {
    setOnlyMyEntitiesFilterAttribute(value);
    this.applyFilters();
  };

  switchNeedApproval = value => {
    setNeedApprovalFilterAttribute(value);
    this.applyFilters();
  };

  applyFilters = () => {
    this.setState({
      dataDisplayed: searchFilters(data, this.props.onlyMyData),
    });
  };

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const { dataDisplayed } = this.state;
    const { lastUpdatedEntity } = this.state;
    const { optionsStatusCascader } = this.state;

    const { loading } = this.props;
    const { entity } = this.props;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      hideDefaultSelections: true,
      selections: [
        {
          key: 'all-data',
          text: 'Select All Data',
          onSelect: () => {
            this.setState({
              selectedRowKeys: [...Array(46).keys()], // 0...45
            });
          },
        },
      ],
    };

    let rowLastUpdatedDate = null;
    if (lastUpdatedEntity) {
      rowLastUpdatedDate = (
        <Row>
          <Col offset={18} span={8}>
            <span>LAST UPDATE :</span>
            <span className={styles.labelLastUpdate}>
              {lastUpdatedEntity.updatedDate.toLocaleDateString(
                'en-US',
                getDateFullString(),
              )}
            </span>
          </Col>
        </Row>
      );
    }

    return (
      <section>
        {rowLastUpdatedDate}
        <Row className={styles.spaceBetweenFiltersTable}>
          <Col offset={1} span={1}>
            <Input onChange={this.editIDFilter} allowClear placeholder="ID" />
          </Col>
          <Col offset={1} span={4}>
            <RangePicker format={dateFormat} onChange={this.editDateFilter} />
          </Col>
          <Col offset={1} span={2}>
            <Cascader
              options={optionsStatusCascader}
              onChange={this.editStatusFilter}
              placeholder="Status"
            />
          </Col>
          <Col offset={1} span={2}>
            <span className={styles.labelSwitch}>Only mine</span>
            <Switch
              checkedChildren={
                <Icon className={styles.iconButtonCreate} type="check" />
              }
              onChange={this.switchOnlyMine}
            />
          </Col>
          <Col offset={1} span={3}>
            <span className={styles.labelSwitch}>Need my approval</span>
            <Switch
              checkedChildren={
                <Icon className={styles.iconButtonCreate} type="check" />
              }
              onChange={this.switchNeedApproval}
            />
          </Col>
          <Col offset={3} span={1}>
            <Button
              type="primary"
              shape="round"
              onClick={this.props.clickNewEntity}
            >
              <Icon type="plus" className={styles.iconButtonCreate} />
              Create {`${entity.key}`}
            </Button>
          </Col>
        </Row>
        <Table
          size="small"
          loading={loading}
          rowSelection={rowSelection}
          columns={getColumns(this.props)}
          dataSource={dataDisplayed}
        />
      </section>
    );
  }
}

EntityListTable.propTypes = {
  entity: PropTypes.object,
  loading: PropTypes.bool,
  data: PropTypes.array,
  onlyMyData: PropTypes.array,
  clickNewEntity: PropTypes.func,
};

export default EntityListTable;
