import React from 'react';
import { FormattedNumber } from 'react-intl';
import PropTypes from 'prop-types';
import { APITofloat } from 'api/expense/helper/format';

export const Currency = ({ value, format, ...rest }) => (
  // <span {...rest}>
  <FormattedNumber
    value={format ? format(value) : value}
    format="currency"
    {...rest}
  />
  // </span>
);

Currency.defaultProps = {
  format: APITofloat,
};

Currency.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  format: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

export default Currency;
