import React from 'react';
import { Card, Col, Empty, Row, Skeleton } from 'antd';
import { AutoSizer, List } from 'react-virtualized';
import PropTypes from 'prop-types';
import styles from './style.less';
import { Currency } from '../Format';

const defaultAttributesToDisplay = [];
const defaultOffsetColumnsSupported = [3, 13];
const defaultWidthColumn = 4;
const defaultRowHeight = 40;
const defaultNumberRowsRendered = 8;
const defaultTextNoData = 'No data';

class CardListData extends React.Component {
  state = {
    offsetColumnsSupported: defaultOffsetColumnsSupported,
    widthColumn: defaultWidthColumn,
    rowHeight: defaultRowHeight,
    numberRowsRendered: defaultNumberRowsRendered,
    textNoData: defaultTextNoData,
    attributesToDisplay: defaultAttributesToDisplay,
  };

  constructor(props) {
    super(props);

    if (props.offsetColumnsSupported)
      this.state.offsetColumnsSupported = props.offsetColumnsSupported;

    if (props.widthColumn) this.state.widthColumn = props.widthColumn;

    if (props.textNoData) this.state.textNoData = props.textNoData;

    if (props.rowHeight) this.state.rowHeight = props.rowHeight;

    if (props.numberRowsRendered)
      this.state.numberRowsRendered = props.numberRowsRendered;

    if (props.attributesToDisplay)
      this.state.attributesToDisplay = props.attributesToDisplay;
  }

  renderRow = ({ index, key, style }) => {
    const columns = getColumns(
      this.props.data,
      index,
      this.state.attributesToDisplay,
      this.state.offsetColumnsSupported,
      this.state.widthColumn,
    );
    return (
      <Row
        className={styles.row}
        key={key}
        style={style}
        onClick={() => this.props.clickOnRow(this.props.data[index])}
      >
        {columns}
      </Row>
    );
  };

  getNumberRowRendered = () => {
    // Securizing minimum before size list
    if (this.props.data.length < this.state.numberRowsRendered) {
      return this.props.data.length;
    }
    return this.state.numberRowsRendered;
  };

  noRowsRenderer = () => (
    <Empty
      image={Empty.PRESENTED_IMAGE_SIMPLE}
      description={<span>{this.state.textNoData}</span>}
    />
  );

  render() {
    const {
      titleCard,
      styleCard,
      headStyleCard,
      bodyStyleCard,
      extraCard,
      actionsCard,
      loading,
    } = this.props;
    let { data } = this.props;
    const {
      attributesToDisplay,
      offsetColumnsSupported,
      widthColumn,
      rowHeight,
    } = this.state;

    if (
      data.length < 1 ||
      getColumns(
        data,
        0,
        attributesToDisplay,
        offsetColumnsSupported,
        widthColumn,
      ).length === 0
    ) {
      data = [];
    }

    return (
      <Card
        title={titleCard}
        style={styleCard}
        headStyle={headStyleCard}
        bodyStyle={bodyStyleCard}
        extra={extraCard}
        actions={actionsCard}
      >
        <Skeleton loading={loading} avatar active>
          <AutoSizer>
            {({ width, height }) => (
              <List
                className={styles.list}
                height={height}
                width={width}
                overscanRowCount={this.getNumberRowRendered()}
                noRowsRenderer={this.noRowsRenderer}
                rowCount={data.length}
                rowHeight={rowHeight}
                rowRenderer={this.renderRow}
              />
            )}
          </AutoSizer>
        </Skeleton>
      </Card>
    );
  }
}

function getColumns(
  dataToTest,
  index,
  attributesToDisplay,
  offsetColumnsSupported,
  widthColumn,
) {
  const columns = [];
  let counter = [];
  // We force our table to take only the two first values from the data passed into our component
  Object.keys(dataToTest[index]).forEach(keyName => {
    if (attributesToDisplay.includes(keyName) && counter < 2) {
      if (typeof dataToTest[index][keyName] === 'number') {
        columns.push(
          <Col
            offset={offsetColumnsSupported[counter]}
            span={widthColumn}
            key={Math.random()}
          >
            <div className={styles.bold}>
              <Currency value={dataToTest[index][keyName]} />
            </div>
          </Col>,
        );
      } else {
        columns.push(
          <Col
            offset={offsetColumnsSupported[counter]}
            span={widthColumn}
            key={Math.random()}
          >
            <div className={styles.bold}>{dataToTest[index][keyName]}</div>
          </Col>,
        );
      }
      ++counter;
    }
  });
  return columns;
}

CardListData.propTypes = {
  titleCard: PropTypes.string,
  styleCard: PropTypes.object,
  headStyleCard: PropTypes.object,
  bodyStyleCard: PropTypes.object,
  extraCard: PropTypes.object,
  actionsCard: PropTypes.array,
  loading: PropTypes.bool,
  data: PropTypes.array,
  attributesToDisplay: PropTypes.array,
  textNoData: PropTypes.string,
  offsetColumnsSupported: PropTypes.array,
  widthColumn: PropTypes.number,
  rowHeight: PropTypes.number,
  numberRowsRendered: PropTypes.number,
  clickOnRow: PropTypes.func,
};

export default CardListData;
