import React from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Icon, Modal, Row, Switch } from 'antd';
import styles from './style.less';
import { Entities } from '../../model/Entities';
import { Currency } from '../Format';

export const PopupEntity = ({
  entity,
  entityType,
  title,
  visible,
  loadingButton,
  onOkText,
  onOkCallback,
  onCancelText,
  onCancelCallback,
  isAcceptPopup,
  onChangeAutoApprovalBox,
}) => {
  const type = isAcceptPopup ? 'accept' : 'deny';

  const autoApproveInfos = isAcceptPopup ? (
    <section>
      You can click the checkbox below to auto-approve the potential invoice
      regarding that {entityType.key}.
      <div className={styles.autoApprovalDiv}>
        <span>Enable Auto-Approval : </span>
        <Switch
          className={styles.switchPosition}
          checkedChildren={<Icon className={styles.iconButtons} type="check" />}
          unCheckedChildren={
            <Icon className={styles.iconButtons} type="close" />
          }
          onChange={onChangeAutoApprovalBox}
        />
      </div>
    </section>
  ) : null;

  const footer = (
    <Row key="footer-popup">
      <Col offset={1} span={10}>
        <Button
          className={styles.buttonSizeVariable}
          key="back"
          onClick={onCancelCallback}
        >
          {onCancelText}
        </Button>
      </Col>
      <Col offset={1} span={10}>
        <Button
          className={styles.buttonSizeVariable}
          key="submit"
          type="primary"
          loading={loadingButton}
          onClick={onOkCallback}
        >
          {onOkText}
        </Button>
      </Col>
    </Row>
  );

  return (
    <div>
      <Modal
        width={700}
        visible={visible}
        title={title}
        onOk={onOkCallback}
        onCancel={onCancelCallback}
        footer={footer}
      >
        <section>
          You are about to{' '}
          <span className={styles.typeTextDescription}>{type}</span> the{' '}
          <span className={styles.typeTextDescription}>
            {`${entityType.key} #${entity.id}`}
          </span>
          . The amount is{' '}
          <span className={styles.amount}>
            <Currency value={entity.amount} />
          </span>
          .{autoApproveInfos}
        </section>
      </Modal>
    </div>
  );
};

PopupEntity.defaultProps = {
  entity: {
    id: 'N/A',
    fiducialEntity: 'N/A',
    budgetAllowance: 0,
    budgetCode: 'N/A',
    frequency: 'N/A',
    glAccount: 'N/A',
    paymentMethod: 'N/A',
    vendors: ['N/A'],
    dueDate: new Date(),
    creationDate: new Date(),
    payer: 'N/A',
    creator: 'N/A',
  },
  entityType: Entities.RFE,
  title: 'Test',
  visible: true,
  loadingButton: false,
  onOkText: 'Validate',
  onOkCallback: null,
  onCancelText: 'Cancel',
  onCancelCallback: null,
  isAcceptPopup: true,
  onChangeAutoApprovalBox: null,
};

PopupEntity.propTypes = {
  entity: PropTypes.object,
  entityType: PropTypes.object,
  title: PropTypes.string,
  visible: PropTypes.bool,
  loadingButton: PropTypes.bool,
  onOkText: PropTypes.string,
  onOkCallback: PropTypes.func,
  onCancelText: PropTypes.string,
  onCancelCallback: PropTypes.func,
  isAcceptPopup: PropTypes.bool,
  onChangeAutoApprovalBox: PropTypes.func,
};
