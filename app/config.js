export const EXPENSE_API = 'http://localhost';

export const AMC_API = 'https://devamcsettings.fiducial.com';

export const FC_PARENT_URL = 'https://devamc.fiducial.com';

export const FC_CLIENT_ID = '5d1cccc00d1ee11cdcd1b297';

export const DISABLE_LOCALE_STORAGE_TOKEN_IN = 1800;
