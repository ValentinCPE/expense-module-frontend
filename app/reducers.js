/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import appReducer from 'stores/app/reducer';
import authReducer from 'stores/auth/reducer';
import { RESET_APP } from 'stores/app/constants';

import languageProviderReducer from 'containers/LanguageProvider/reducer';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
export const routeInitialState = fromJS({
  location: null,
});

/**
 * Merge route into the global application state
 */
export function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default injectedReducers => (state, action) =>
  combineReducers({
    route: routeReducer,
    language: languageProviderReducer,
    // stores
    app: appReducer,
    auth: authReducer,
    ...injectedReducers,
  })(action.type === RESET_APP ? undefined : state, action);
