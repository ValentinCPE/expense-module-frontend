/**
 * Filter for entity
 */
const criteriaFilters = {
  id: '',
  dateBeginning: null,
  dateEnding: null,
  status: '',
  onlyMyEntities: false,
  needApproval: false,
};

/**
 * Fill in all the Criteria Filter Object
 * @param id
 * @param dateBeginning
 * @param dateEnding
 * @param status
 * @param onlyMyEntities
 * @param needApproval
 */
export const setCriteriaFiltersAttribute = (
  id,
  dateBeginning,
  dateEnding,
  status,
  onlyMyEntities,
  needApproval,
) => {
  criteriaFilters.id = id;
  criteriaFilters.dateBeginning = dateBeginning;
  criteriaFilters.dateEnding = dateEnding;
  criteriaFilters.status = status;
  criteriaFilters.onlyMyEntities = onlyMyEntities;
  criteriaFilters.needApproval = needApproval;
};

export const searchFilters = (dataFetched, onlyMyData) => {
  const filteredWords = [];
  if (dataFetched.length > 0) {
    const searchWordToLowerCase = criteriaFilters.id.toLowerCase(); // cache result function to speed the search up

    // cache size table with variable
    for (let i = 0, l = dataFetched.length; i < l; ++i) {
      const currentEntity = dataFetched[i];
      if (
        currentEntity.id.toLowerCase().includes(searchWordToLowerCase) &&
        (!criteriaFilters.dateBeginning ||
          (criteriaFilters.dateBeginning &&
            currentEntity.creationDate >= criteriaFilters.dateBeginning)) &&
        (!criteriaFilters.dateEnding ||
          (criteriaFilters.dateEnding &&
            currentEntity.creationDate <= criteriaFilters.dateEnding)) &&
        (!criteriaFilters.status ||
          (criteriaFilters.status &&
            currentEntity.status === criteriaFilters.status)) &&
        (!criteriaFilters.onlyMyEntities ||
          (criteriaFilters.onlyMyEntities &&
            onlyMyData.indexOf(currentEntity) !== -1)) &&
        (!criteriaFilters.needApproval ||
          (criteriaFilters.needApproval && currentEntity.needApproval))
      ) {
        currentEntity.key = `rowFiltered${i}`;
        filteredWords.push(currentEntity);
      }
    }
  } else {
    return dataFetched;
  }

  return filteredWords;
};

/**
 * Fill in id attribute
 * @param id
 */
export const setIdFilterAttribute = id => {
  criteriaFilters.id = id;
};

/**
 * Fill in dateBeginning attribute
 * @param dateBeginning
 */
export const setDateBeginningFilterAttribute = dateBeginning => {
  criteriaFilters.dateBeginning = dateBeginning;
};

/**
 * Fill in dateEnding attribute
 * @param dateEnding
 */
export const setDateEndingFilterAttribute = dateEnding => {
  criteriaFilters.dateEnding = dateEnding;
};

/**
 * Fill in status attribute
 * @param status
 */
export const setStatusFilterAttribute = status => {
  criteriaFilters.status = status;
};

/**
 * Fill in onlyMyEntities attribute
 * @param onlyMyEntities
 */
export const setOnlyMyEntitiesFilterAttribute = onlyMyEntities => {
  criteriaFilters.onlyMyEntities = onlyMyEntities;
};

/**
 * Fill in needApproval attribute
 * @param needApproval
 */
export const setNeedApprovalFilterAttribute = needApproval => {
  criteriaFilters.needApproval = needApproval;
};
