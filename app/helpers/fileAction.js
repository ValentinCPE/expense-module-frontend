import { FileTypeKnown, FileTypeUnknown } from '../model/FileType';

/**
 * Return good FileType according format file passed in param
 * @param format : string
 * @returns {object}
 */
export const getFileTypeAccordingFormat = format => {
  const fileType = [];
  Object.keys(FileTypeKnown).some(type => {
    if (type === format.toUpperCase()) {
      fileType.push(FileTypeKnown[type]);
    }
    // Stop execution when the length of array > 0
    return fileType.length > 0;
  });
  if (fileType.length === 0) {
    fileType.push(FileTypeUnknown);
  }
  const [firstElt] = fileType;
  return firstElt;
};
