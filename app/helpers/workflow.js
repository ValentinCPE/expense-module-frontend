import { Steps } from 'antd';
import React from 'react';
import { Status } from '../model/Status';

const { Step } = Steps;

/**
 * Get step Workflow according to currentStatus
 *
 * @param currentStatus : Status - Current Status of the entity
 * @param sizeSteps : String - Size of Steps component (small or default)
 *
 * Return React Component representing an entity workflow
 */
export const getWorkflowSteps = (currentStatus, sizeSteps) => {
  const steps = [];
  let indexStatus = 0;
  let statusFound = false;

  Object.keys(Status).forEach((item, index) => {
    if (!statusFound && Status[item].label === currentStatus) {
      indexStatus = index;
      statusFound = true;
    }
    steps.push(
      <Step
        key={Status[item].textDisplayed}
        title={Status[item].textDisplayed}
      />,
    );
  });

  return (
    <Steps
      key="workflow-status"
      current={indexStatus}
      status="process"
      labelPlacement="vertical"
      size={sizeSteps}
    >
      {steps}
    </Steps>
  );
};
