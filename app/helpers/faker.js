export const createRFE = number => {
  const rfes = [];
  const now = new Date();
  const attachment = {
    name: 'devis',
    type: 'pdf',
  };

  const comment1 = {
    id: 1,
    creationDate: new Date(),
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    username: 'Nicolas',
    attachments: [attachment, attachment],
    logo: 'user',
  };
  const comment2 = {
    id: 2,
    creationDate: new Date(),
    text: 'Test commentaire',
    username: 'Florian',
    attachments: [attachment],
    logo: 'user',
  };

  for (let i = 0; i < number; ++i) {
    const newDate = new Date(now);
    const obj = {
      key: `row${i + 1}`,
      // Generate a random UUID
      id: `${newDate.getFullYear().toString() +
        newDate.getMonth().toString() +
        newDate.getDate().toString()}JC${i + 1}`,
      amount: i % 2 === 0 ? 850 : 0,
      attachments: i % 2 === 0 ? [attachment, attachment] : [],
      fiducialEntity: 'QQC',
      budgetAllowance: 5000000,
      budgetCode: 'CODE',
      frequency: 'Every month',
      glAccount: 'test',
      paymentMethod: 'Credit card',
      vendors: ['test1', 'test2'],
      dueDate: new Date(newDate.getDate() + 5),
      payer: 'Accountant',
      creator: 'John Doe',
      creationDate: newDate,
      updatedDate: new Date(),
      status: i % 2 === 0 ? 'DRAFT' : 'SUBMIT',
      needApproval: i % 2 !== 0,
      comments: [comment1, comment2],
    };

    rfes.push(obj);
    now.setDate(now.getDate() - 1);
  }

  return rfes;
};

export const getOneRandomRFE = id => {
  const rfes = createRFE(1);
  const [rfe] = rfes;
  rfe.id = id;
  return rfe;
};
