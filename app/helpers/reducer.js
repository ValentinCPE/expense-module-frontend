import { fromJS } from 'immutable';
import { isEmpty, every, get } from 'lodash';
import refArrayIndex from 'helpers/refArrayIndex';
import { toJS } from 'helpers/immutable';

export const injectLoadingState = { loading: false };
export const injectErrorState = { error: false };

export const injectDefaultState = {
  ...injectLoadingState,
  ...injectErrorState,
};

export const onLoad = state => state.set('loading', true).set('error', null);

export const onSuccess = state => state.set('loading', false);

export const onError = (state, action = {}) =>
  state
    .set('error', toJS(action.error || `Unknown error for ${action.type}`))
    .set('loading', false);

/**
 *
 * @param {List} immutableList
 * @param {object} params
 * @param {object} mapper use path of property as key (_.get)
 */
export const substractListFromParams = (
  immutableList,
  params = {},
  mapper = {},
) => {
  if (!immutableList.size || isEmpty(params)) return immutableList;
  // const formatedParams = isEmpty(mapper)
  //   ? params
  //   : reduce(
  //     mapper,
  //     (memo, fn, key) => ({ ...memo, [key]: fn(params[key]) }),
  //     ...params
  //   );
  return immutableList.filterNot(obj => {
    const src = obj.toJSON();
    const match = every(params, (val, getKey) => {
      const srcVal = get(src, getKey);
      const formatedSrcVal = getKey in mapper ? mapper[getKey](srcVal) : srcVal;
      return formatedSrcVal === val;
    });
    return match;
  });
};

/**
 *
 * @param {List} immutableList
 * @param {array} data
 * @param {object} params
 * @param {object} mapper use path of property as key (_.get)
 */
export const updateListFromParams = (
  immutableList,
  data,
  params = {},
  mapper = {},
) => {
  const compiledData = Array.isArray(data) ? data : [data];
  return substractListFromParams(immutableList, params, mapper).concat(
    fromJS(compiledData),
  );
};

export const listToSet = (immutableList, key) =>
  fromJS(refArrayIndex(toJS(immutableList), key));
