/**
 *
 * @param {string|Map} val
 * @returns {array}
 */
export const mapToArray = (val, byKey) =>
  byKey ? mapToArrayByKey(val) : mapToArrayByValues(val);

export const mapToArrayByKey = val => Object.keys(toJS(val));
export const mapToArrayByValues = val => Object.values(toJS(val));

/**
 *
 * @param {object|Map} val
 * @param {any} def value by default
 * @returns {object}
 */
export const toJS = (val, def = {}) =>
  (val && val.toJS ? val.toJS() : val) || def;

/**
 *
 * @param {Array|List} val
 * @param {any} def value by default
 * @returns {object}
 */
export const toArray = (val, def = []) =>
  (val && val.toArray ? val.toArray() : val) || def;

/**
 *
 * @param {Immutable} state
 * @param {string} key
 * @param {*object} defValue
 * @returns {array}
 */
export const getMapToArray = (
  state,
  key,
  { defaultValue = {}, byKey = true } = {},
) => mapToArray(state.get(key, defaultValue), byKey);

export const getMapToArrayByKey = (state, key, defaultValue) =>
  getMapToArray(state, key, { defaultValue, byKey: true });
export const getMapToArrayByValues = (state, key, defaultValue) =>
  getMapToArray(state, key, { defaultValue, byKey: false });

export const selectorToJS = def => v => toJS(v, def);
