/**
 * Compare 2 objects by their indexes
 * @export
 * @param {object} src
 * @param {object} target
 * @returns {boolean}
 */
export default (src = {}, target = {}) => {
  const srcVals = Object.values(src);
  const targetVals = Object.values(target);
  return (
    srcVals.length === targetVals.length &&
    srcVals.every(index => index in target)
  );
};
