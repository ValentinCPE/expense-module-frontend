import { isFunction, isEmpty, isNumber } from 'lodash';

const PLURALIZE_TPL_CFG = {
  OPEN: '{{',
  SEPARATOR: /\|/,
  CLOSE: '}}',
  // --- Because of separator being a regex we can't use the .length attribute :(
  OPEN_LGTH: 2,
  SEPARATOR_LGTH: 1,
  CLOSE_LGTH: 2,
};
export const aOrAn = nextWord => (/^[aeiouy]/i.test(nextWord[0]) ? 'an' : 'a');

export const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

/**
 * Example:
 *  from: '{{He\'s|They are}} happy'
 *  with plural: true
 *      'He's happy'
 *  with plural: false
 *      'They are happy'
 * @param {String} from
 * @param {Boolean} plural
 * @param {Object} cfg
 * @return {String}
 */
function extractPluralizeTemplateInformation(from, plural, cfg) {
  const data = {};
  data.open = from.search(cfg.OPEN);
  if (data.open === -1) {
    return null;
  }
  data.separator = from.search(cfg.SEPARATOR);
  if (data.separator === -1) {
    throw new Error('Separator is missing');
  }
  data.close = from.search(cfg.CLOSE);
  if (data.close === -1) {
    throw new Error(`Closer ${cfg.CLOSE} is missing`);
  }
  const textReplaced = plural
    ? from.substring(data.separator + cfg.SEPARATOR_LGTH, data.close)
    : from.substring(data.open + cfg.OPEN_LGTH, data.separator);
  return (
    from.substring(0, data.open) +
    textReplaced +
    from.substring(data.close + cfg.CLOSE_LGTH)
  );
}

/**
 * Example:
 *  from: '{{He\'s|They are}} happy'
 *  with nb: 0 or 1
 *      'He's happy'
 *  with nb: > 1
 *      'They are happy'
 * @param {String} src
 * @param {Number} nb
 * @return {String}
 */
export const pluralizeTemplate = (src, nb) => {
  let tmpResult = null;
  const multi = nb > 1;
  let newText = src;
  while (true) {
    // eslint-disable-line no-constant-condition
    tmpResult = extractPluralizeTemplateInformation(
      newText,
      multi,
      PLURALIZE_TPL_CFG,
    );
    if (tmpResult === null) {
      break;
    }
    newText = tmpResult;
  }
  return newText;
};

/**
 * [pluralize description]
 * @method pluralize
 * @param  {String}  singularText [description]
 * @param  {Number}  nb           [description]
 * @param  {Object}  cfg         [description]
 * @return {String}               [description]
 */
export const pluralize = (singularText, nb, cfg = {}) => {
  if (isEmpty(singularText)) {
    console.warn('singularText is required', singularText);
    return '';
  }
  if (!isNumber(nb)) {
    console.warn('nb is required', nb);
    return singularText;
  }

  const opts = {
    addNumber: null, // null, 'prefix', 'suffix'
    addNumberPlural: true,
    addNumberSingular: true,
    addNumberNegate: false,
    addNumberFn: function addNumberFn(result, number, options) {
      if (options.addNumber === 'prefix') return `${number} ${result}`;
      return `${result} ${number}`;
    },
    pluralizeText: function pluralizeText(text) {
      return `${text}s`;
    },
    negateText: function negateText(text, options) {
      if (options.addNumber === null || !options.addNumberNegate)
        return `no ${text}`;
      return text;
    },
    ...cfg,
  };
  if (nb === 1 || nb === -1) {
    return opts.addNumber === null || !opts.addNumberSingular
      ? singularText
      : opts.addNumberFn(singularText, nb, opts);
  }
  if (nb === 0) {
    const result = isFunction(opts.negateText)
      ? opts.negateText(singularText, opts)
      : opts.negateText;
    return opts.addNumber === null || !opts.addNumberNegate
      ? result
      : opts.addNumberFn(result, nb, opts);
  }
  const result = isFunction(opts.pluralizeText)
    ? opts.pluralizeText(singularText, opts)
    : opts.pluralizeText;
  return opts.addNumber === null || !opts.addNumberPlural
    ? result
    : opts.addNumberFn(result, nb, opts);
};

export const truncate = (str, maxLength, overflowTxt = '...') => {
  let result = str;
  if (str.length > maxLength) {
    result = str.slice(0, maxLength);
    result = str.slice(0, str.length - overflowTxt.length) + overflowTxt;
  }
  return result;
};
