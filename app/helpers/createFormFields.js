/**
 *
 * @param {string} properties properties to map
 * @param {object} srcProp
 * @param {object} formProp
 * @param {antd/Form} form
 */
export const createFormField = (
  property,
  srcProp,
  formProp,
  form,
  srcPropMapper = {},
) => {
  const formData = formProp[property];

  const fieldsOpts = {
    ...formData,
  };
  if (formData && !formData.touched) {
    if (property in srcPropMapper) {
      fieldsOpts.value = srcPropMapper[property](srcProp);
    } else if (property in srcProp) {
      fieldsOpts.value = srcProp[property];
    }
  }
  return form.createFormField(fieldsOpts);
};

/**
 * Set Form values by default based on an srcProp
 * @param {string[]} properties properties to map
 * @param {object} srcProp
 * @param {object} formProp
 * @param {antd/Form} form
 * @param {object} srcPropMapper
 */
/* eslint-disable no-param-reassign */
export const createFormFields = (
  properties = [],
  srcProp,
  formProp,
  form,
  srcPropMapper,
) =>
  properties.reduce((memo, property) => {
    memo[property] = createFormField(
      property,
      srcProp,
      formProp,
      form,
      srcPropMapper,
    );
    return memo;
  }, {});

export default createFormFields;
