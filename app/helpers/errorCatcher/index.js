import { ErrorNotification } from 'fiducial-react-components/es';
import { notAuthenticated } from 'stores/auth/actions';
import { get, uniqueId, debounce } from 'lodash';
import { notification } from 'antd';

const authNotification = () =>
  notification.warn({ message: 'Authentification error', key: 'auth-error' });

const debouncedAuthNotification = debounce(authNotification, 3000, {
  leading: true,
  trailing: false,
});

function showError(err = null, store) {
  // Handle XHR errors
  if (err && err.error && err.error.response && err.error.response.json) {
    if (err.error.response.status === 403) {
      debouncedAuthNotification();
      store.dispatch(notAuthenticated());
    } else {
      err.error.response
        .json()
        .then(res => {
          const error = (res ? res.information || res.message : null) || null;
          ErrorNotification.show({
            key: get(err, 'title', uniqueId('err_')),
            ...(err || {}),
            ...(error ? { error } : {}),
            reload: true,
            // onReloadConfirm: () => onReloadConfirm(store),
          });
        })
        .catch(() => {
          ErrorNotification.show({
            key: get(err, 'title', uniqueId('err_')),
            ...(err || {}),
            reload: true,
            // onReloadConfirm: () => onReloadConfirm(store),
          });
        });
    }
  } else {
    ErrorNotification.show({
      key: get(err, 'title', uniqueId('err_')),
      ...(err || {}),
      reload: true,
      // onReloadConfirm: () => onReloadConfirm(store),
    });
  }
}

export default typeCatcher => store => next => action => {
  try {
    if (action.type && action.type === typeCatcher) {
      showError(action && action.error, store);
    }
    return next(action);
  } catch (err) {
    console.trace(action, err); // eslint-disable-line no-console
    showError(err, store);
    throw err;
  }
};
