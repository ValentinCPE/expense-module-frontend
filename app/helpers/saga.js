import { fork, take, call } from 'redux-saga/effects';
/* eslint-disable redux-saga/yield-effects */
export const takeLeading = (patternOrChannel, saga, ...args) =>
  fork(function* gen() {
    while (true) {
      const action = yield take(patternOrChannel);
      yield call(saga, ...args.concat(action));
    }
  });
