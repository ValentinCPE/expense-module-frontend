import { isBefore, endOfToday, startOfToday } from 'date-fns';

/**
 * Is the Date before earlier than today?
 */
export const isBeforeToday = (endOfTheDay = true) => date =>
  date && isBefore(date, endOfTheDay ? endOfToday() : startOfToday());

export const getDateFullString = () => ({
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
});
