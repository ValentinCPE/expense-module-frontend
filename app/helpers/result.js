import { isFunction } from 'lodash';

/**
 * If val is a function, it will be called with the arguments
 * @export
 * @param {any} val
 * @param {any} args function arguments
 * @returns {any}
 */
export function getValue(val, ...args) {
  return isFunction(val) ? val.apply(this, args) : val;
}
