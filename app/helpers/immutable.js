import { get } from 'lodash';
import { fromJS, List } from 'immutable';
import refArrayIndex from 'helpers/refArrayIndex';

/**
 *
 * @param {object|Map} val
 * @param {any} def value by default
 * @returns {object}
 */
export const toJS = (val, def = {}) =>
  (val && val.toJS ? val.toJS() : val) || def;

/**
 *
 * @param {Array|List} val
 * @param {any} def value by default
 * @returns {object}
 */
export const toArray = (val, def = []) =>
  (val && val.toArray ? val.toArray() : val) || def;

/**
 *
 * @param {List} listSrc
 * @param {array} data
 * @param {string} key
 * @param {Map} mapRef
 */
export const addOrMergeToListByKey = (
  listSrc = List(),
  data = [],
  key = 'id',
  mapRef = null,
) => {
  const refByKey = mapRef || fromJS(refArrayIndex(toJS(listSrc), key));
  return data.reduce((memo, val) => {
    const id = String(get(val, key));
    let index = null;
    // Already exist
    if (refByKey && refByKey.has(id)) {
      index = mapRef.get(id);
      return memo.set(index, fromJS(val));
      // New
    }
    return memo.push(fromJS(val));
  }, listSrc);
};
