import { get } from 'lodash';

/**
 * @example refArrayIndex([{ id: 1 }, { id: 3 }, { id: 2 }], 'id') => { 1: 0, 2: 2, 3: 1 }
 * @param {array} val
 * @param {string} key
 * @returns {object}
 */
const refArrayIndex = (val, key) =>
  val.reduce((memo, row, index) => ({ ...memo, [get(row, key)]: index }), {});

export default refArrayIndex;
