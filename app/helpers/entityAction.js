/**
 * Return Last Updated entity from entities array
 * @param entities : array of entity
 * @returns entity : object representing Entity (RFE, Invoice, RFR...)
 */
export const getLastUpdatedEntityFromArray = entities => {
  let lastUpdatedEntity = null;
  entities.forEach(entity => {
    if (
      !lastUpdatedEntity ||
      entity.updatedDate > lastUpdatedEntity.updatedDate
    ) {
      lastUpdatedEntity = entity;
    }
  });
  return lastUpdatedEntity;
};
