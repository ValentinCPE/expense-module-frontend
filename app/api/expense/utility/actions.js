import { PING } from './constants';

export function ping() {
  return {
    type: PING.LOAD,
  };
}

export function pingSuccess() {
  return {
    type: PING.SUCCESS,
  };
}

export function pingError() {
  return {
    type: PING.ERROR,
  };
}
