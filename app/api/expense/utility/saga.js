/**
 * Gets the repositories of the user from Github
 */

import request from 'utils/request';

import { call, put, takeLatest } from 'redux-saga/effects';

import { PING as PING_TYPE } from './constants';
import { pingSuccess, pingError } from './actions';
import { buildURI } from '../_internal/helper';

function* ping() {
  try {
    const requestURL = buildURI('/api/provider/me/ping');
    const header = {
      method: 'POST',
    };
    const response = yield call(request, requestURL, header);
    yield put(pingSuccess(response));
    return { response };
  } catch (error) {
    yield call(pingError, error);
    return { error };
  }
}
function* pingSaga() {
  yield takeLatest(PING_TYPE.LOAD, ping);
}

export default {
  ping,
  pingSaga,
};
