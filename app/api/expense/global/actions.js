import { GLOBAL } from './constants';

export function getError(error) {
  return {
    type: GLOBAL.ERROR,
    error,
  };
}
