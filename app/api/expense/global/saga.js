import { call, put, all } from 'redux-saga/effects';
import { getError } from './actions';

export function formatError(error) {
  try {
    // XHR Error
    if (
      error instanceof Error &&
      error.response &&
      error.response instanceof Response
    ) {
      const { status, statusText } = error.response;
      return {
        description: `${status}: ${statusText}`,
        error,
      };
    }
    return error;
  } catch (_) {
    return error;
  }
}

const defaultonError = { throwGlobal: true, extraInfo: {}, title: null };

export function* onError(error, errorAction, options = defaultonError) {
  const { throwGlobal, extraInfo, title } = { ...defaultonError, ...options };
  const formatedError = yield call(formatError, error);
  if (title) {
    formatedError.title = title;
  }
  yield all([
    put(errorAction({ ...extraInfo, ...formatedError })),
    ...(throwGlobal ? [put(getError(formatedError))] : []),
  ]);
}
