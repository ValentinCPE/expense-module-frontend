export default class Model {
  constructor(data = {}) {
    Object.keys(data).forEach(val => {
      this[val] = data[val];
    });
  }
}
