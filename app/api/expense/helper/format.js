import { format as formatFns } from 'date-fns';
import moment from 'moment';
import { isDate } from 'lodash';
import config from '../_internal/config';

/**
 * Return the formatted date string.
 * @param {string|date} date
 * @param {string} format https://date-fns.org/v1.29.0/docs/format
 * @return {string} the formatted date
 */
export const dateToString = (date, format = config.DATE_FORMAT) =>
  date ? formatFns(isDate(date) ? date : new Date(date), format) : null;

/**
 * Return the formatted moment date as string.
 * @param {moment} date
 * @param {string} format https://momentjs.com/docs/#/displaying/format/
 * @return {string} the formatted date
 */
export const momentToString = (momentDate, format = config.DATE_FORMAT) =>
  moment.isMoment(momentDate) ? momentDate.format(format) : momentDate;

/**
 * Prepare a dateRange to be formated to a query filter
 * @param {moment[]|string[]} dateRange ['10/10/2010', '10/11/2010']
 * @param {object} options
 */
export const dateRangeToQueryFilter = (
  dateRange,
  { fromInclusive = true, toInclusive = true } = {},
) => {
  const from = momentToString(dateRange[0] ? dateRange[0] : moment());
  const to = momentToString(dateRange[1] ? dateRange[1] : moment());
  // if (from === to && fromInclusive && toInclusive) return from;
  return `${fromInclusive ? '[' : ']'}${from};${to}${toInclusive ? ']' : '['}`;
};

/**
 * Float -> API
 * @example 3.00 -> 300
 * @param {integer} val
 * @returns {number}
 */
export const floatToAPI = val => Number((Number(val || 0) * 100).toFixed(2));

/**
 * API -> Float
 * @example 300 -> 3.00
 * @param {float} val API value
 * @returns {number}
 */
export const APITofloat = val => Number(val || 0) / 100;

/**
 * API -> moment
 * @param {string} val API date value
 * @returns {moment}
 */
export const APIToMoment = val => moment(new Date(val));

/**
 * @example ['1'] => '1', ['1', '2'] => ['1', '2']
 * @param {array} val
 * @returns {array|string}
 */
export const arrayOrSingle = val =>
  Array.isArray(val) && val.length === 1 ? val[0] : val;
