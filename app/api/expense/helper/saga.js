import request from 'utils/request';
import { all, call, put } from 'redux-saga/effects';

import { isRequired, buildURI, createErrorTitleFor } from '../_internal/helper';
import { onError } from '../global/saga';

/**
 *
 * @export
 * @param {object} [{
 *   url = isRequired('url is Required'),
 *   successAction = isRequired('successAction is Required'),
 *   errorAction = isRequired('errorAction is Required'),
 *   name = isRequired('name is Required'),
 *   filter = null, // Add params to the url based on the property name
 * }={}]
 * @returns {func}
 */
export function createGet({
  url = isRequired('url is Required'),
  successAction = isRequired('successAction is Required'),
  errorAction = isRequired('errorAction is Required'),
  name = isRequired('name is Required'),
  filter = null,
} = {}) {
  return function* _createGet(params) {
    const requestURL = buildURI(url(params), filter ? params[filter] : {});
    try {
      const result = yield call(request, requestURL);
      yield put(successAction(result, params));
    } catch (err) {
      yield call(onError, err, errorAction, {
        title: createErrorTitleFor.GET(name),
        extraInfo: params,
      });
    }
  };
}
export function createParallelGet({
  url = isRequired('url is Required'),
  successAction = isRequired('successAction is Required'),
  errorAction = isRequired('errorAction is Required'),
  name = isRequired('name is Required'),
  filter = null,
  parallelWith = isRequired('parallelWith is Required'),
} = {}) {
  return function* _createGet(params) {
    try {
      const newParams = { ...params };
      delete newParams[parallelWith];
      const src = params[parallelWith];
      const result = yield all(
        (Array.isArray(src) ? src : [src]).map(parallelValue => {
          const finalParams = {
            ...newParams,
            [parallelWith]: parallelValue,
          };
          const requestURL = buildURI(
            url(finalParams),
            filter ? finalParams[filter] : {},
          );
          return call(request, requestURL);
        }),
      );
      // const result = yield call(request, requestURL);
      yield put(successAction(result, params));
    } catch (err) {
      yield call(onError, err, errorAction, {
        title: createErrorTitleFor.GET(name),
        extraInfo: params,
      });
    }
  };
}
