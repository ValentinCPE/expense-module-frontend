import { keyBy, each } from 'lodash';

export class ResourceEntry {
  constructor(resourceProp, resourceKeyId, reflect = {}) {
    this.resourceProp = resourceProp;
    this.resourceKeyId = resourceKeyId;
    this.reflect = reflect;
  }
  addReflect(from, to) {
    this.reflect[from] = to;
    return this;
  }
  mergeReflect(resourceEntry) {
    each(resourceEntry.reflect, (to, from) => this.addReflect(from, to));
    return this;
  }
  removeReflect(from) {
    delete this.reflect[from];
  }
  clone() {
    return new ResourceEntry(
      this.resourceProp,
      this.resourceKeyId,
      this.reflect,
    );
  }
  get key() {
    return this.resourceProp;
  }
}

export class ResourceMapper {
  /**
   *
   * @param {ResourceEntry[]} entries
   */
  constructor(entries = []) {
    this.entries = {};
    const arrayOfEntries = Array.from(entries) ? entries : [entries];
    arrayOfEntries.forEach(this.add);
    return this;
  }
  /**
   *
   * @param {ResourceEntry|string} resourceProp
   * @param {string} resourceKeyId
   * @param {string} reflect
   */
  add(resourceProp, resourceKeyId, reflect) {
    let to = null;
    if (!(resourceProp instanceof ResourceEntry)) {
      to = new ResourceEntry(resourceProp, resourceKeyId, reflect);
    } else {
      to = resourceProp;
    }
    // We merge the reflectors
    if (this.has(to)) {
      this.get(to).mergeReflect(to);
    } else {
      this.entries[this.getResourceEntryKey(to)] = to;
    }
    return this;
  }
  /**
   * @param {ResourceEntry} resourceEntry
   * @return {string}
   */
  getResourceEntryKey = resourceEntry => resourceEntry.key;
  /**
   * @param {ResourceEntry|string} key
   * @return {string}
   */
  getKey = key =>
    key instanceof ResourceEntry ? this.getResourceEntryKey(key) : key;
  /**
   * @param {ResourceEntry|string} key
   * @return {ResourceEntry}
   */
  get = key => this.entries[this.getKey(key)];
  /**
   * @param {ResourceEntry|string} key
   * @return {bool}
   */
  has = key => this.getKey(key) in this.entries;
  /**
   * @param {ResourceEntry|string} key
   */
  remove(key) {
    delete this.entries[this.getKey(key)];
    return this;
  }
}

const RESOURCES_ENTRIES = {
  ENTITY: new ResourceEntry('entity', 'entityId'),
  USER: new ResourceEntry('user', 'userId'),
};

export const getResourceEntry = key =>
  RESOURCES_ENTRIES[key] ? RESOURCES_ENTRIES[key].clone() : null;

export const TEMPLATES = {
  CUSTOMER_ID_TO_ENTITY: RESOURCES_ENTRIES.ENTITY.clone().addReflect(
    'customerId',
    'entity',
  ),
  USER_ID_TO_USER: RESOURCES_ENTRIES.USER.clone().addReflect('userId', 'user'),
  UPDATED_BY_TO_UPDATER: RESOURCES_ENTRIES.USER.clone().addReflect(
    'updatedBy',
    'updater',
  ),
  CREATED_BY_TO_CREATOR: RESOURCES_ENTRIES.USER.clone().addReflect(
    'createdBy',
    'creator',
  ),
};

export const DEFAULT_RESOURCE_MAP = new ResourceMapper()
  .add(TEMPLATES.CUSTOMER_ID_TO_ENTITY)
  .add(TEMPLATES.USER_ID_TO_USER)
  .add(TEMPLATES.UPDATED_BY_TO_UPDATER)
  .add(TEMPLATES.CREATED_BY_TO_CREATOR);

/**
 * @param {ResourceMapper|ResourceEntry|ResourceEntry[]} src Describe how to parse the data
 * @return {ResourceMapper}
 */
const paramToResourceMapper = src =>
  src instanceof ResourceMapper ? src : new ResourceMapper(src);

/**
 * Add the linked resource directly to the data objet
 * @param {object} response XHR Body response
 * @param {ResourceMapper|ResourceEntry|ResourceEntry[]} config Describe how to parse the data
 */
export const addResources = (
  { data = {}, resource = [] },
  config = DEFAULT_RESOURCE_MAP,
) => {
  const returnArray = Array.isArray(data);
  const copiedData = returnArray ? [...data] : [data];

  const rescMap = paramToResourceMapper(config);
  /* eslint-disable array-callback-return, consistent-return */
  each(
    resource,
    (resourceData, key) => {
      if (!rescMap.has(key)) {
        return this;
      }
      const { resourceKeyId, reflect } = rescMap.get(key);
      const hash = keyBy(resourceData, resourceKeyId) || {};
      /* eslint-disable no-param-reassign */
      each(reflect, (to, src) => {
        each(copiedData, value => {
          value[to] = { ...hash[value[src]] } || {};
        });
      });
    },
    {},
  );
  return returnArray ? copiedData : copiedData[0];
};
