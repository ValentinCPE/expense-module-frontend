/**
 * Gets the repositories of the user from Github
 */

import request from 'utils/request';

import { call, put, takeLatest } from 'redux-saga/effects';

import {
  GET_CUSTOMER,
  GET_CUSTOMERS,
  GET_CUSTOMER_USERS,
  GET_PERMISSION,
  GET_PROFILE,
  AUTH,
  URLS,
} from './constants';
import {
  getCustomerSuccess,
  getCustomerError,
  getCustomersSuccess,
  getCustomersError,
  getCustomerUsersSuccess,
  getCustomerUsersError,
  authSuccess,
  authError,
  getPermissionSuccess,
  getPermissionError,
  getProfileSuccess,
  getProfileError,
} from './actions';

import { isRequired, buildURI, createErrorTitleFor } from '../_internal/helper';
import { createGet } from '../helper/saga';
import { onError } from '../global/saga';

const getCustomer = createGet({
  url: URLS.GET_CUSTOMER,
  successAction: getCustomerSuccess,
  errorAction: getCustomerError,
  name: 'Client',
});

function* getCustomerSaga() {
  yield takeLatest(GET_CUSTOMER.LOAD, getCustomer);
}

const getCustomers = createGet({
  url: URLS.GET_CUSTOMER,
  successAction: getCustomersSuccess,
  errorAction: getCustomersError,
  name: 'Client',
});

function* getCustomersSaga() {
  yield takeLatest(GET_CUSTOMERS.LOAD, getCustomers);
}

function* getCustomerUsers({
  customerId = isRequired('customerId is Required'),
  filter = {},
} = {}) {
  const requestURL = buildURI(
    `/api/provider/amc/entity/${customerId}/users`,
    filter,
  );
  try {
    const result = yield call(request, requestURL);
    yield put(getCustomerUsersSuccess(result));
  } catch (err) {
    yield call(onError, err, getCustomerUsersError);
  }
}

function* getCustomerUsersSaga() {
  yield takeLatest(GET_CUSTOMER_USERS.LOAD, getCustomerUsers);
}

export function* authenticate({ username, password }) {
  try {
    const header = {
      method: 'POST',
      body: JSON.stringify({
        grant_type: 'password',
        username,
        password,
      }),
    };
    const requestURL = buildURI('/api/provider/amc/token');
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, header);
    yield put(
      authSuccess(result.access_token, result.refresh_token, result.expires_in),
    );
  } catch (err) {
    yield call(onError, err, authError);
  }
}

function* authenticateSaga() {
  yield takeLatest(AUTH.LOAD, authenticate);
}

export function* getPermission() {
  try {
    const requestURL = buildURI('/api/provider/amc/me/permission');
    const result = yield call(request, requestURL);
    yield put(getPermissionSuccess(result));
  } catch (err) {
    yield call(onError, err, getPermissionError, {
      title: createErrorTitleFor.GET('Permission'),
    });
  }
}

function* getPermissionSaga() {
  yield takeLatest(GET_PERMISSION.LOAD, getPermission);
}

export function* getProfile() {
  try {
    const requestURL = buildURI('/api/provider/amc/me/profile');
    const result = yield call(request, requestURL);
    yield put(getProfileSuccess(result));
  } catch (err) {
    yield call(onError, err, getProfileError, {
      title: createErrorTitleFor.GET('Profile'),
    });
  }
}

function* getProfileSaga() {
  yield takeLatest(GET_PROFILE.LOAD, getProfile);
}

export default {
  authenticate,
  authenticateSaga,
  getCustomer,
  getCustomerSaga,
  getCustomers,
  getCustomersSaga,
  getCustomerUsers,
  getCustomerUsersSaga,
  getPermission,
  getPermissionSaga,
  getProfile,
  getProfileSaga,
};
