import { fromJS } from 'immutable';

import {
  LOAD_AUTH_BY_ACCESS_TOKEN,
  CURRENT_CUSTOMER,
  LOAD_CURRENT_PERMISSION,
  LOAD_CURRENT_PERMISSION_SUCCESS,
  LOAD_CURRENT_PERMISSION_ERROR,
  LOAD_ON_LOGIN_SUCCESS,
  LOAD_ON_LOGIN_ERROR,
  FETCH_USER_PROFILE,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_ERROR,
  AUTH_FORBIDDEN,
  AUTH,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  accessToken: null,
  refreshToken: null,
});

function onLoad(state) {
  return state.set('loading', true).set('error', false);
}

function onSuccess(state) {
  return state.set('loading', false);
}

function onError(state, action) {
  return state.set('error', action.error).set('loading', false);
}

function appReducer(state = initialState, action) {
  switch (action.type) {
    case CURRENT_CUSTOMER:
      return state.set('currentCustomer', fromJS(action.customer));
    case AUTH.LOAD:
      return onLoad(state)
        .set('accessToken', null)
        .set('refreshToken', null);
    case AUTH.SUCCESS:
      return onSuccess(state)
        .set('accessToken', action.accessToken)
        .set('refreshToken', action.refreshToken);
    case LOAD_AUTH_BY_ACCESS_TOKEN:
      return state.set('accessToken', null).set('refreshToken', null);
    case AUTH_FORBIDDEN:
      return state.set('accessToken', null).set('refreshToken', null);
    case LOAD_CURRENT_PERMISSION:
      return onLoad(state).set('currentPermission', {});
    case LOAD_CURRENT_PERMISSION_SUCCESS:
      return onSuccess(state).set(
        'currentPermission',
        fromJS(action.permission),
      );
    case LOAD_ON_LOGIN_SUCCESS:
      return state.set('ready', true);
    case FETCH_USER_PROFILE:
      return onLoad(state).set('isProfileLoading', true);
    case FETCH_USER_PROFILE_SUCCESS:
      return onSuccess(state)
        .set('isProfileLoading', false)
        .set('currentUser', fromJS(action.profile));
    case AUTH.ERROR:
    case LOAD_ON_LOGIN_ERROR:
    case LOAD_CURRENT_PERMISSION_ERROR:
    case FETCH_USER_PROFILE_ERROR:
      return onError(state, action);
    default:
      return state;
  }
}

export default appReducer;
