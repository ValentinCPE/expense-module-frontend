import { createTypes } from '../_internal/helper';

export const GET_CUSTOMERS = createTypes('GET_CUSTOMERS');

export const GET_CUSTOMER = createTypes('GET_CUSTOMER');

export const GET_CUSTOMER_USERS = createTypes('GET_CUSTOMER_USERS');

export const AUTH = createTypes('AUTH');

export const GET_PERMISSION = createTypes('GET_PERMISSION');

export const GET_PROFILE = createTypes('GET_PROFILE');

export const URLS = {
  GET_CUSTOMER: ({ entityId }) => `/api/provider/amc/entity/${entityId}`,
  GET_CUSTOMERS: () => '/api/provider/amc/entity',
};
