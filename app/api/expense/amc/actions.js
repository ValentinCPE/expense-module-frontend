import {
  AUTH,
  GET_CUSTOMER,
  GET_CUSTOMERS,
  GET_CUSTOMER_USERS,
  GET_PERMISSION,
  GET_PROFILE,
} from './constants';

export function auth(username, password) {
  return {
    type: AUTH.LOAD,
    username,
    password,
  };
}

export function authSuccess(accessToken, refreshToken, expiresIn) {
  return {
    type: AUTH.SUCCESS,
    accessToken,
    refreshToken,
    expiresIn,
  };
}

export function authError(error) {
  return {
    type: AUTH.ERROR,
    error,
  };
}

export function getCustomer(entityId) {
  return {
    type: GET_CUSTOMER.LOAD,
    entityId,
  };
}

export function getCustomerSuccess(resp, { entityId }) {
  return {
    type: GET_CUSTOMER.SUCCESS,
    resp,
    entityId,
  };
}

export function getCustomerError(error) {
  return {
    type: GET_CUSTOMER.ERROR,
    error,
  };
}

export function getCustomers(term = null) {
  const filter = {};
  if (term) {
    filter.q = term;
  }
  return {
    type: GET_CUSTOMERS.LOAD,
    filter,
  };
}

export function getCustomersSuccess(resp, { filter }) {
  return {
    type: GET_CUSTOMERS.SUCCESS,
    resp,
    filter,
  };
}

export function getCustomersError(error) {
  return {
    type: GET_CUSTOMERS.ERROR,
    error,
  };
}

export function getCustomerUsers(customerId, term = null) {
  const filter = {};
  if (term) {
    filter.q = term;
  }
  return {
    type: GET_CUSTOMER_USERS.LOAD,
    customerId,
    filter,
  };
}

export function getCustomerUsersSuccess(resp) {
  return {
    type: GET_CUSTOMER_USERS.SUCCESS,
    resp,
  };
}

export function getCustomerUsersError(error) {
  return {
    type: GET_CUSTOMER_USERS.ERROR,
    error,
  };
}

export function getPermission() {
  return {
    type: GET_PERMISSION.LOAD,
  };
}

export function getPermissionSuccess(resp) {
  return {
    type: GET_PERMISSION.SUCCESS,
    resp,
  };
}

export function getPermissionError(error) {
  return {
    type: GET_PERMISSION.ERROR,
    error,
  };
}

export function getProfile() {
  return {
    type: GET_PROFILE.LOAD,
  };
}

export function getProfileSuccess(resp) {
  return {
    type: GET_PROFILE.SUCCESS,
    resp,
  };
}

export function getProfileError(error) {
  return {
    type: GET_PROFILE.ERROR,
    error,
  };
}
