class ApiConfig {
  constructor() {
    this.DATE_FORMAT = 'MM/DD/YYYY';
    this.API_URL = '';
    this.ERROR_MESSAGE = (action, method) =>
      `Sorry, we were unable to ${action} the ${method}.`;
    this.ACTION_NAMESPACE = '@@expenseAPI';
    this.ERROR_ACTION = {
      GET: 'GET',
      GET_ALL: 'GET_ALL',
      CREATE: 'CREATE',
      UPDATE: 'UPDATE',
      REMOVE: 'REMOVE',
    };

    this.ERROR_ACTIONS = {
      [this.ERROR_ACTION.GET]: 'get',
      [this.ERROR_ACTION.GET_ALL]: 'get All',
      [this.ERROR_ACTION.CREATE]: 'create',
      [this.ERROR_ACTION.UPDATE]: 'update',
      [this.ERROR_ACTION.REMOVE]: 'remove',
    };
  }
  setURL(v) {
    this.API_URL = v;
    return this;
  }
  setErrorMessage(v) {
    this.ERROR_MESSAGE = v;
    return this;
  }
}

const config = new ApiConfig();
export default config;
