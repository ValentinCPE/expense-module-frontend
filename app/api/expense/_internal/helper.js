import { reduce, uniqueId, isEmpty } from 'lodash';
import config from './config';

/**
 * @typedef {Object} ActionType
 * @property {string} LOAD
 * @property {string} SUCCESS
 * @property {string} ERROR
 */

/**
 * Error title creation
 * @param {string} action
 * @param {method} method
 */
export const createErrorTitle = (action, method) =>
  config.ERROR_MESSAGE(action, method);

export const createErrorTitleFor = Object.keys(config.ERROR_ACTIONS).reduce(
  (memo, action) => ({
    ...memo,
    [action]: method => createErrorTitle(config.ERROR_ACTIONS[action], method),
  }),
  {},
);

export const METHODS = {
  POST: 'POST',
  GET: 'GET',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

const STATES = {
  LOAD: 'LOAD',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

const getUniqueNameSpace = () => uniqueId('u');

export const createUniqueType = (name, state = STATES.LOAD) =>
  `${config.ACTION_NAMESPACE}/${getUniqueNameSpace()}/${name}_${state}`;

export const createType = (nameSpace, name, state = STATES.LOAD) =>
  `${config.ACTION_NAMESPACE}/${nameSpace}/${name}_${state}`;

/**
 *
 *
 * @export
 * @param {any} name
 * @returns {ActionType}
 */
export function createTypes(name) {
  const nameSpace = getUniqueNameSpace();
  /* eslint-disable no-param-reassign */
  return reduce(
    STATES,
    (memo, stateVal, stateKey) => {
      memo[stateKey] = createType(nameSpace, name, stateKey);
      return memo;
    },
    {},
  );
}
/**
 * Add parameters to an URL
 * Ex:
 *  addQueryParam('/api/v1/debt', { test: 1, yes: 2 }) => '/api/v1/debt?test=1&yes=2'
 *  addQueryParam('/api/v1/debt?', { test: 1, yes: 2 }) => '/api/v1/debt?test=1&yes=2'
 * @export addQueryParam
 * @param {string} url
 * @param {object} params
 * @returns {string}
 */
export const addQueryParam = (url, params = {}) =>
  url +
  (url.indexOf('?') === -1 && !isEmpty(params) ? '?' : '') +
  queryParams(params);

export const queryParams = (obj, prefix) =>
  Object.keys(obj || {})
    .map(key => {
      const prefixedKey = prefix ? `${prefix}[]` : key; // PHP will handle the array index by himself
      // const prefixedKey = prefix ? `${prefix}[${key}]` : key;
      const value = obj[key];
      return value !== null && typeof value === 'object'
        ? queryParams(value, prefixedKey)
        : `${encodeURIComponent(prefixedKey)}=${encodeURIComponent(value)}`;
    })
    .join('&');

export const formatMomentToAPI = momentDate =>
  momentDate ? momentDate.format(config.DATE_FORMAT) : null;

export function isRequired(msg = 'Parameter not supplied') {
  throw new Error(msg);
}

export const buildURI = (method = isRequired(), params = {}) =>
  addQueryParam(`${config.API_URL}${method}`, params);
