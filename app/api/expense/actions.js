import * as amc from './amc/actions';
import * as global from './global/actions';
import * as utility from './utility/actions';

export { amc, global, utility };
