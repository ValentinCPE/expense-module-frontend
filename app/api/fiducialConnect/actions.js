import { START, TOKEN_CHANGE } from './constants';

export function start() {
  return {
    type: START.LOAD,
  };
}
export function startSuccess() {
  return {
    type: START.SUCCESS,
  };
}
export function startError() {
  return {
    type: START.ERROR,
  };
}

export function tokenChangeSuccess(data) {
  return {
    type: TOKEN_CHANGE.SUCCESS,
    data,
  };
}
