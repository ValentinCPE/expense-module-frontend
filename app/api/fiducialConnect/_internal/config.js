import { FDClient } from 'amc-client-library/fiducial.connect.common.client';

class ApiConfig {
  constructor() {
    this.DATE_FORMAT = 'MM/DD/YYYY';
    this.PARENT_URL = '';
    this.CLIENT_ID = '';
    this.ERROR_MESSAGE = (action, method) =>
      `Sorry, we were unable to ${action} a ${method}.`;
    this.ACTION_NAMESPACE = '@@fconnect';
    this.ERROR_ACTION = {
      GET: 'GET',
      GET_ALL: 'GET_ALL',
      CREATE: 'CREATE',
      UPDATE: 'UPDATE',
      REMOVE: 'REMOVE',
    };
    this.FDClient = FDClient; // Parent of the iframe
    this.ERROR_ACTIONS = {
      [this.ERROR_ACTION.GET]: 'get',
      [this.ERROR_ACTION.GET_ALL]: 'get All',
      [this.ERROR_ACTION.CREATE]: 'create',
      [this.ERROR_ACTION.UPDATE]: 'update',
      [this.ERROR_ACTION.REMOVE]: 'remove',
    };
  }
  setParentURL(v) {
    this.PARENT_URL = v;
    this.FDClient.setParentURL(this.PARENT_URL);
    return this;
  }
  setClientId(v) {
    this.CLIENT_ID = v;
    this.FDClient.openApplication(this.CLIENT_ID);
    return this;
  }
  setErrorMessage(v) {
    this.ERROR_MESSAGE = v;
    return this;
  }
}

const config = new ApiConfig();
export default config;
