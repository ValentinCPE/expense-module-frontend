import { createTypes } from './_internal/helper';

export const START = createTypes('START');

export const TOKEN_CHANGE = createTypes('TOKEN_CHANGE');
