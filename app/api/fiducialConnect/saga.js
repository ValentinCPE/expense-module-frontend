/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

import Config from './_internal/config';

import { startSuccess, startError, tokenChangeSuccess } from './actions';
import { START } from './constants';

const EVENTS = {
  CHANGE_TOKEN: 'change:token',
};

function createFConnectChannel() {
  return eventChannel(emitter => {
    Config.FDClient.on(EVENTS.CHANGE_TOKEN, (...params) => {
      emitter({ event: EVENTS.CHANGE_TOKEN, data: params });
    });
    return () => {};
  });
}

export function* start() {
  let chan = null;
  try {
    chan = yield call(createFConnectChannel);
    yield put(startSuccess());
  } catch (err) {
    yield put(startError(err));
  }
  while (true) {
    const { event, data } = yield take(chan);
    if (event === EVENTS.CHANGE_TOKEN) {
      yield put(tokenChangeSuccess(data));
    }
  }
}

function* startSaga() {
  yield takeLatest(START.LOAD, start);
}

export default {
  start,
  startSaga,
};
