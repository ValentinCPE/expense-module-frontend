import { isRequired, buildAvatURI } from '../_internal/helper';

export const getAvatarURI = (entityId = isRequired(), size) =>
  buildAvatURI(`/api/images/entity/${entityId}`, size);
