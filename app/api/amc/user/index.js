import { isRequired, buildAvatURI } from '../_internal/helper';

export const getAvatarURI = (userId = isRequired(), size) =>
  buildAvatURI(`/api/images/user/${userId}`, size);
