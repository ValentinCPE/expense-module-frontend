export const EXTRA_PERMISSION = {
  CUSTOMER_LIST_ENABLED: '_customerListEnabled',
};

export const ROLES = {
  ADMIN: 'is_admin',
  EMPLOYEE: 'is_employee',
  EMPLOYEE_IT: 'is_employee_it',
};
