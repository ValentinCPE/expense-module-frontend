import React from 'react';

import { Row, Col, Icon } from 'antd';
import { DASHBOARD } from 'routes';
import { Page } from 'fiducial-react-components/es';
import styles from './style.less';
import CardListData from '../../components/CardListData';
import { createRFE } from '../../helpers/faker';

const path = [DASHBOARD];

class Dashboard extends React.Component {
  state = {
    data: [],
    loading: true,
  };

  componentDidMount() {
    this.setState({ data: createRFE(300), loading: false });
  }

  static seeDetailRFE(/* rfe */) {
    // Redirect URL RFE
  }

  render() {
    const { loading } = this.state;

    return (
      <Page clearBackground path={path} title="Dashboard">
        <div className={styles.title_dashboard}>
          <span>Waiting for review</span>
        </div>

        <Row className={styles.spaceBelowCards}>
          <Col offset={1} span={6}>
            <CardListData
              titleCard="Request For Expense"
              styleCard={{ width: '100%', marginTop: 16 }}
              headStyleCard={{
                backgroundColor: '#9eb681',
                color: 'white',
                fontWeight: 'bold',
              }}
              bodyStyleCard={!loading ? { padding: 0, height: '15vh' } : {}}
              extraCard={
                [].length <= 0 ? null : (
                  <Icon
                    type="alert"
                    theme="filled"
                    style={{
                      fontSize: '20px',
                      color: 'white',
                    }}
                  />
                )
              }
              actionsCard={[<span>View</span>, <span>New</span>]}
              loading={loading}
              data={[]}
              offsetColumnsSupported={[1, 5]}
              clickOnRow={Dashboard.seeDetailRFE}
            />
          </Col>

          <Col offset={2} span={6}>
            <CardListData
              titleCard="Invoice"
              styleCard={{ width: '100%', marginTop: 16 }}
              headStyleCard={{
                backgroundColor: '#9b7f79',
                color: 'white',
                fontWeight: 'bold',
              }}
              bodyStyleCard={!loading ? { padding: 0, height: '15vh' } : {}}
              extraCard={
                [].length <= 0 ? null : (
                  <Icon
                    type="alert"
                    theme="filled"
                    style={{
                      fontSize: '20px',
                      color: 'white',
                    }}
                  />
                )
              }
              actionsCard={[<span>View</span>, <span>New</span>]}
              loading={loading}
              data={[]}
              textNoData="No data"
              clickOnRow={Dashboard.seeDetailRFE}
            />
          </Col>
          <Col offset={2} span={6}>
            <CardListData
              titleCard="Request For Reimbursement"
              styleCard={{ width: '100%', marginTop: 16 }}
              headStyleCard={{
                backgroundColor: '#666e71',
                color: 'white',
                fontWeight: 'bold',
              }}
              bodyStyleCard={!loading ? { padding: 0, height: '15vh' } : {}}
              extraCard={
                this.state.data.length <= 0 ? null : (
                  <Icon
                    type="alert"
                    theme="filled"
                    style={{
                      fontSize: '20px',
                      color: 'white',
                    }}
                  />
                )
              }
              actionsCard={[<span>View</span>, <span>New</span>]}
              loading={loading}
              data={this.state.data}
              attributesToDisplay={['id', 'amount']}
              clickOnRow={Dashboard.seeDetailRFE}
            />
          </Col>
        </Row>
      </Page>
    );
  }
}

export default Dashboard;
