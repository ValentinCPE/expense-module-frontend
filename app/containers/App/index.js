/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import { isEqual, isEmpty } from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import PermissionRoute from 'containers/PermissionRoute/Loadable';
import { createStructuredSelector } from 'reselect';
import { Layout, Popover, notification, Icon } from 'antd';
import {
  Loader,
  Layout as FiduLayout,
  ErrorNotification,
} from 'fiducial-react-components/es';

import Menu from 'containers/Menu';
import { init } from 'stores/app/actions';
import { SELECTORS } from 'stores/app';
import { isAuthenticated } from 'stores/auth/selectors';
import { EXTRA_PERMISSION } from 'permission';
import { getMenuTree, getRoutes, PATH } from 'routes';
import saga from 'stores/app/saga';
import styles from './style.less';

const {
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  isReady,
  makeSelectCurrentPermission,
  makeSelectCurrentCustomer,
} = SELECTORS;

const { Sider } = Layout;
const { Footer, Header } = FiduLayout;

class App extends React.Component {
  state = {
    error: null,
    // showWelcomeMessage: false,
    isReady: false,
  };
  static getDerivedStateFromProps(nextProps, prevState) {
    let newState = null;
    if (nextProps.isReady !== prevState.isReady) {
      newState = {
        ...newState,
        isReady: nextProps.isReady,
        // showWelcomeMessage: true,
      };
    }
    // } else if (prevState.isReady) {
    //   newState = { ...newState, showWelcomeMessage: false };
    // }
    if (!isEqual(nextProps.error, prevState.error)) {
      newState = { ...newState, error: nextProps.error };
    }
    return newState;
  }
  componentDidMount() {
    this.props.init();
    return this;
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.isReady && !prevProps.isReady) {
      notification.info({
        icon: <Icon type="like-o" className={styles.welcome} />,
        message: (
          <span>Hi {this.props.user.firstname}, how are you today?</span>
        ),
        placement: 'bottomRight',
        key: 'welcome-msg',
      });
    }
    if (this.state.error && !prevState.error) {
      ErrorNotification.show(this.state.error);
    }
  }
  componentDidCatch(error) {
    this.setState({
      error,
    });
  }
  // shouldComponentUpdate(nextProps) {
  //   if (nextProps.isReady !== this.props.isReady) {
  //     return true;
  //   } else if (
  //     nextProps.isReady &&
  //     (nextProps.customer !== this.props.customer ||
  //       !_.isEqual(this.props.permission, nextProps.permission)
  //     )
  //   ) {
  //     return true;
  //   }
  //   return false;
  // }
  onCollapseToggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  getMenuPermissionFromComponent() {
    const result = {};
    if (!isEmpty(this.props.customer)) {
      result[EXTRA_PERMISSION.CUSTOMER_LIST_ENABLED] = true;
    }
    return result;
  }
  getMenuPermission() {
    return {
      ...this.getMenuPermissionFromComponent(),
      ...this.props.permission,
    };
  }
  renderRoute = ({ type, path, component, allowed, exact = false }) => (
    <PermissionRoute
      type={type}
      exact={exact}
      key={path}
      path={path}
      component={component}
      allowedPermissions={allowed || []}
    />
  );
  render() {
    const { user } = this.props;

    if (!this.props.isAuthenticated) {
      return <Redirect to={PATH.LOGIN} />;
    }
    if (!this.props.isReady) {
      return <Loader />;
    }

    const userName = user.username || 'Anonymous';
    return (
      <Layout className="layout" style={{ height: '100vh' }}>
        <Header.default
          applicationName="Expense"
          userName={userName}
          collapsible
          onCollapseToggle={this.onCollapseToggle}
        />
        <Layout>
          <Sider
            width={250}
            style={{ background: '#fff' }}
            collapsible
            collapsed={this.state.collapsed}
            trigger={null}
          >
            <Menu tree={getMenuTree()} permissions={this.getMenuPermission()} />
          </Sider>
          <Layout>
            <Switch>{getRoutes().map(this.renderRoute)}</Switch>
            <Footer
              version={
                <Popover
                  placement="topLeft"
                  title="Technical Software Information"
                >
                  {/* eslint-disable-next-line no-undef */}
                  <span>{BUILD.APP_VERSION}</span>
                </Popover>
              }
            />
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

App.propTypes = {
  /* eslint-disable react/no-unused-prop-types */
  error: PropTypes.any,
  user: PropTypes.object,
  customer: PropTypes.object,
  init: PropTypes.func,
  isAuthenticated: PropTypes.bool,
  permission: PropTypes.object,
  isReady: PropTypes.bool,
  throwError: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectCurrentUser(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  isAuthenticated: isAuthenticated(),
  permission: makeSelectCurrentPermission(),
  customer: makeSelectCurrentCustomer(),
  isReady: isReady(),
});

export function mapDispatchToProps(dispatch) {
  return {
    init: () => dispatch(init()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export const SAGA = { key: 'app', saga };

const withSaga = injectSaga([SAGA]);

export default compose(
  withSaga,
  withConnect,
)(App);
