import { createSelector } from 'reselect';
import { routeInitialState } from 'reducers';

const selectRoute = state => state.get('route', routeInitialState);

const makeSelectLocation = () =>
  createSelector(
    selectRoute,
    routeState => routeState.get('location').toJS(),
  );

export { makeSelectLocation };
