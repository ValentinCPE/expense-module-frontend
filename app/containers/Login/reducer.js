import { fromJS } from 'immutable';

import { AUTH } from 'api/expense/amc/constants';

export const initialState = fromJS({
  isAuthLoading: false,
  username: null,
  password: null,
});

function reducer(state = initialState, action) {
  switch (action.type) {
    case AUTH.LOAD:
      return state
        .set('isAuthLoading', true)
        .set('username', action.username)
        .set('password', action.password);
    case AUTH.SUCCESS:
      return state
        .set('isAuthLoading', false)
        .set('username', null)
        .set('password', null);
    case AUTH.ERROR:
      return state
        .set('isAuthLoading', false)
        .set('username', null)
        .set('password', null);
    default:
      return state;
  }
}

export default reducer;
