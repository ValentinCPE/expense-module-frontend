import { takeLatest } from 'redux-saga/effects';

import { start } from 'api/fiducialConnect/saga';
import { START } from 'api/fiducialConnect/constants';

/**
 * Root saga manages watcher lifecycle
 */
export default function* appData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(START.LOAD, start);
}
