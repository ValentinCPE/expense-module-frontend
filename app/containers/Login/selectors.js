/**
 * The global state selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.get('login', initialState);

const isAuthentificationLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.get('isAuthLoading'),
  );

const getUsername = () =>
  createSelector(
    selectLogin,
    loginState => loginState.get('username'),
  );

const getPassword = () =>
  createSelector(
    selectLogin,
    loginState => loginState.get('password'),
  );

export { selectLogin, isAuthentificationLoading, getUsername, getPassword };
