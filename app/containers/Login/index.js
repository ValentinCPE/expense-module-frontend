import React from 'react';
import PropTypes from 'prop-types';
// import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import { Row, Form, Input, Button, Alert } from 'antd';
import {
  isAuthenticated,
  // isAuthCheckingLocalStorage,
  makeSelectError,
  makeSelectLoading,
} from 'stores/auth/selectors';
import { isReady } from 'stores/app/selectors';
import { auth } from 'api/expense/amc/actions';
// import RedirectToHomepage from 'containers/RedirectToHomepage';
import saga from './saga';
// import { PATH } from 'routes';
import reducer from './reducer';
import styles from './style.less';

// import messages from './messages';

const FormItem = Form.Item;

class Login extends React.Component {
  // componentDidMount() {
  //   this.props.authLocalStorage();
  // }
  onSubmit = () => {
    this.props.form.validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      this.props.authenticate(values.username, values.password);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const fieldsDisabled = this.props.isAuthentificationLoading;
    return (
      <div className={styles.form}>
        {/* <Helmet>
          <title><FormattedMessage {...messages.title} /></title>
        </Helmet> */}
        <div className={styles.logo}>
          <span>Login</span>
        </div>
        <Form onSubmit={this.onSubmit}>
          <FormItem hasFeedback>
            {getFieldDecorator('username', {
              rules: [
                {
                  required: true,
                },
              ],
            })(
              <Input
                size="large"
                autoComplete="username"
                placeholder="Username"
                disabled={fieldsDisabled}
              />,
            )}
          </FormItem>
          <FormItem hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                },
              ],
            })(
              <Input
                size="large"
                autoComplete="current-password"
                type="password"
                placeholder="Password"
                disabled={fieldsDisabled}
              />,
            )}
          </FormItem>
          <Row>
            <Button
              type="primary"
              size="large"
              loading={this.props.isAuthentificationLoading}
              disabled={fieldsDisabled}
              htmlType="submit"
            >
              Sign in
            </Button>
          </Row>
        </Form>
        {this.props.error && (
          <Row>
            <Alert
              description="Wrong account or password."
              type="error"
              className={styles.error}
              showIcon
              closable
            />
          </Row>
        )}
      </div>
    );
  }
}

Login.propTypes = {
  error: PropTypes.any,
  form: PropTypes.object,
  isAuthentificationLoading: PropTypes.bool,
  /* eslint-disable react/no-unused-prop-types */
  isAuthenticated: PropTypes.bool,
  ready: PropTypes.bool,
  // isAuthCheckingLocalStorage: PropTypes.bool,
  authenticate: PropTypes.func,
  location: PropTypes.object,
  // currentUser: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  isAuthentificationLoading: makeSelectLoading(),
  ready: isReady(),
  isAuthenticated: isAuthenticated(),
  // isAuthCheckingLocalStorage: isAuthCheckingLocalStorage(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    authenticate: (...params) => dispatch(auth(...params)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  Form.create(),
)(Login);
