/*
 * FeaturePage Messages
 *
 * This contains all the text for the FeaturePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'boilerplate.containers.Login.title',
    defaultMessage: 'Debt List',
  },
});
