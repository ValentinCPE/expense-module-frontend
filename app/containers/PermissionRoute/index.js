import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Page, Restricted } from 'fiducial-react-components/es';

import { createStructuredSelector } from 'reselect';

import { makeSelectCurrentPermission } from 'stores/app/selectors';
import { isAuthenticated } from 'stores/auth/selectors';
import { PATH } from 'routes';

class PermissionRoute extends React.Component {
  getRenderContent(Component, props, entityType) {
    if (!this.props.public && !this.props.isAuthenticated) {
      return (
        <Redirect
          to={{
            pathname: PATH.LOGIN,
            state: { from: props.location },
          }}
        />
      );
    }
    return <Component entityType={entityType} {...props} />;
  }
  isAllowed() {
    const { allowedPermissions } = this.props;
    try {
      if (!allowedPermissions.length) {
        return true;
      }
      return !allowedPermissions.every(
        permissionName => !this.props.permission[permissionName],
      );
    } catch (e) {
      return false;
    }
  }

  render() {
    const { component: Component, type, ...rest } = this.props;
    const accessRestricted =
      !this.props.public && this.props.isAuthenticated && !this.isAllowed();
    if (accessRestricted) {
      return (
        <Page>
          <Restricted />
        </Page>
      );
    }
    return (
      <Route
        {...rest}
        render={props => this.getRenderContent(Component, props, type)}
      />
    );
  }
}

PermissionRoute.propTypes = {
  type: PropTypes.object,
  component: PropTypes.func,
  public: PropTypes.bool, // True: Dot not Require an authentification
  allowedPermissions: PropTypes.array,
  redirectRestrictedTo: PropTypes.string,
  // --
  isAuthenticated: PropTypes.bool,
  permission: PropTypes.object,
};

PermissionRoute.defaultProps = {
  public: false,
  redirectRestrictedTo: '',
  allowedPermissions: [],
  permission: {},
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: isAuthenticated(),
  permission: makeSelectCurrentPermission(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(PermissionRoute);
