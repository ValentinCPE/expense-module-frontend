import React from 'react';
import { Page } from 'fiducial-react-components/es';
import PropTypes from 'prop-types';
import { createRFE } from '../../../helpers/faker';
import EntityListTable from '../../../components/EntityListTable';

let onlyMyRfes = [];

class ListEntityLoaderComponent extends React.Component {
  state = {
    allRFEs: [],
    loading: true,
  };

  componentDidMount() {
    const data = createRFE(980);
    onlyMyRfes = createRFE(20);
    this.setState({
      allRFEs: [...data, ...onlyMyRfes],
      loading: false,
    });
  }

  clickNewRFE = () => {};

  render() {
    const { loading, allRFEs } = this.state;
    const { entityType } = this.props;

    return (
      <Page title={`${entityType.key} List`}>
        <EntityListTable
          entity={entityType}
          loading={loading}
          data={allRFEs}
          onlyMyData={onlyMyRfes}
          clickNewEntity={this.clickNewRFE}
        />
      </Page>
    );
  }
}

ListEntityLoaderComponent.propTypes = {
  entityType: PropTypes.object,
};

export default ListEntityLoaderComponent;
