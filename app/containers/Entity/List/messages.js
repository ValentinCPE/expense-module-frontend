/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'boilerplate.containers.Rfe.title',
    defaultMessage: 'RFELIST',
  },
});
