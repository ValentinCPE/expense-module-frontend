import React from 'react';
import PropTypes from 'prop-types';
import { Loader, Page } from 'fiducial-react-components/es';
import { getOneRandomRFE } from '../../../helpers/faker';

const EntityDetail = React.lazy(() =>
  import('../../../components/EntityDetail'),
);

class DetailEntityLoaderComponent extends React.Component {
  state = {
    rfe: {},
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({
      rfe: getOneRandomRFE(id),
    });
  }

  render() {
    const { rfe } = this.state;
    const { entityType } = this.props;

    const loader = (
      <Loader key="loader-waiting" spinning fullScreen={false}>
        LOADING
      </Loader>
    );
    const entityDetail = [];
    if (Object.entries(rfe).length === 0) {
      entityDetail.push(loader);
    } else {
      entityDetail.push(
        <React.Suspense key="loadingEntityDetail" fallback={loader}>
          <EntityDetail entityType={entityType} entity={rfe} />
        </React.Suspense>,
      );
    }

    return (
      <Page clearBackground title={`${entityType.key} Detail`}>
        {entityDetail}
      </Page>
    );
  }
}

DetailEntityLoaderComponent.propTypes = {
  match: PropTypes.object,
  entityType: PropTypes.object,
};

export default DetailEntityLoaderComponent;
