import React from 'react';

import { Page } from 'fiducial-react-components/es';
import { ADMIN_MENU, ADMIN_DASHBOARD } from '../../routes';

const path = [ADMIN_MENU, ADMIN_DASHBOARD];

export default () => (
  <Page path={path} title="Admin Dashboard">
    <h1>Admin dashboard</h1>
  </Page>
);
