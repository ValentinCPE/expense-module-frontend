import React, { PureComponent } from 'react';
import { Menu as MenuAntd, Icon, Tooltip } from 'antd';
import { withRouter, matchPath } from 'react-router';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './style.less';

function predicatePermission(rules, permissions, allow = true) {
  try {
    if (!rules || !rules.length) {
      return allow;
    }
    return !rules.every(permissionName => !permissions[permissionName]);
  } catch (e) {
    return !allow;
  }
}

function isAllowed(rules, permissions) {
  return predicatePermission(rules, permissions, true);
}

function renderMenuNodeContent(icon, title) {
  return (
    <Tooltip title={title}>
      {icon ? <Icon type={icon} /> : null}
      <span>{title}</span>
    </Tooltip>
  );
}

function renderMenu(
  menuTree,
  permissions,
  currentPath,
  autoExpand,
  result = {},
  parent = null,
) {
  const tree = Array.isArray(menuTree) ? menuTree : [menuTree];
  return tree.map(currentNode => {
    const { subTree, path, icon, title, allowed, key } = currentNode;
    if (!isAllowed(allowed, permissions)) {
      return '';
    }
    if (subTree) {
      return (
        <MenuAntd.SubMenu key={key} title={renderMenuNodeContent(icon, title)}>
          {subTree.map(node =>
            renderMenu(
              node,
              permissions,
              currentPath,
              autoExpand,
              result,
              currentNode,
            ),
          )}
        </MenuAntd.SubMenu>
      );
    }
    if (parent !== null && (autoExpand || matchPath(currentPath, { path }))) {
      /* eslint-disable no-param-reassign */
      result[parent.key] = true;
    }
    return (
      <MenuAntd.Item key={path}>
        <NavLink to={path}>{renderMenuNodeContent(icon, title)}</NavLink>
      </MenuAntd.Item>
    );
  }, this);
}

class Menu extends PureComponent {
  compileTree;
  render() {
    const { location } = this.props;
    const {
      className,
      style,
      header,
      tree,
      permissions,
      footer,
      autoExpand,
    } = this.props;
    const defaultOpenKeys = {};
    const renderedMenu =
      tree &&
      renderMenu(
        tree,
        permissions,
        location.pathname,
        autoExpand,
        defaultOpenKeys,
      );
    return (
      <MenuAntd
        mode="inline"
        className={classNames(styles.menu, className)}
        style={style}
        defaultOpenKeys={Object.keys(defaultOpenKeys)}
      >
        {header ? <MenuAntd.Item>{header}</MenuAntd.Item> : null}
        {renderedMenu}
        {footer ? <MenuAntd.Item>{footer}</MenuAntd.Item> : null}
      </MenuAntd>
    );
  }
}

Menu.propTypes = {
  header: PropTypes.node,
  footer: PropTypes.node,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  tree: PropTypes.PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      path: PropTypes.string,
      icon: PropTypes.string,
      allowed: PropTypes.array,
      subTree: PropTypes.array,
      key: PropTypes.string, // Used for SubTree Parent
    }),
  ).isRequired,
  permissions: PropTypes.objectOf(PropTypes.bool),
  style: PropTypes.object,
  className: PropTypes.string,
  autoExpand: PropTypes.bool,
};

Menu.defaultProps = {
  autoExpand: true,
  header: null,
  footer: null,
  tree: [],
  permissions: {},
  style: {},
  className: '',
};

export default withRouter(Menu);
