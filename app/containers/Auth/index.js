import { omit } from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import { Loader } from 'fiducial-react-components/es';
import Login from 'containers/Login/Loadable';
import App from 'containers/App';
import { entryPoint, authLocalStorage } from 'stores/auth/actions';
import { start } from 'api/fiducialConnect/actions';
import {
  isAuthenticated,
  isAuthCheckingEntryPoint,
  isAuthCheckingLocalStorage,
  makeSelectError,
} from 'stores/auth/selectors';
import saga from 'stores/auth/saga';

const TOKEN_PARAM_KEY = 'token';

class Auth extends React.Component {
  state = {
    error: null,
    isReady: false,
    // isAuthenticated: false,
    entryPointToken: null,
    checkEntryPoint: false,
  };
  componentDidMount() {
    this.props.startFiducialConnect();
    const uriParams = new URLSearchParams(this.props.location.search);
    if (uriParams.has(TOKEN_PARAM_KEY)) {
      this.props.entryPoint(uriParams.get(TOKEN_PARAM_KEY));
    } else {
      this.props.authLocalStorage();
    }
  }
  componentDidCatch(error) {
    this.setState({
      error,
    });
  }

  render() {
    // if (this.props.error || this.state.error) {
    //   return <Login />;
    // }
    if (!this.props.isAuthenticated) {
      if (
        this.props.isAuthCheckingEntryPoint ||
        this.props.isAuthCheckingLocalStorage
      ) {
        return <Loader />;
      }
      return <Login />;
    }
    return (
      <App
        {...omit(this.props, [
          'isAuthenticated',
          'startFiducialConnect',
          'entryPoint',
        ])}
      />
    );
  }
}

Auth.propTypes = {
  isAuthenticated: PropTypes.bool,
  startFiducialConnect: PropTypes.func,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.bool,
  ]),
  entryPoint: PropTypes.func,
  isAuthCheckingEntryPoint: PropTypes.bool,
  isAuthCheckingLocalStorage: PropTypes.bool,
  authLocalStorage: PropTypes.func,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: isAuthenticated(),
  isAuthCheckingEntryPoint: isAuthCheckingEntryPoint(),
  isAuthCheckingLocalStorage: isAuthCheckingLocalStorage(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    startFiducialConnect: () => dispatch(start()),
    entryPoint: accessToken => dispatch(entryPoint(accessToken)),
    authLocalStorage: (...params) => dispatch(authLocalStorage(...params)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withSaga = injectSaga({ key: 'auth', saga });

export default compose(
  withSaga,
  withConnect,
)(Auth);
