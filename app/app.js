/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Switch, Route, HashRouter } from 'react-router-dom';
import { ErrorNotification } from 'fiducial-react-components/es';
import { setGlobalHeader } from 'utils/request';

// Import root app
import Auth from 'containers/Auth';
import createHistory from 'history/createBrowserHistory';
import fiducialConnectAPIConfig from 'api/fiducialConnect/_internal/config';
import expenseAPIConfig from 'api/expense/_internal/config';
import amcAPIConfig from 'api/amc/_internal/config';
import { resetApp } from 'stores/app/actions';
import 'sanitize.css/sanitize.css';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Load the favicon and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import 'file-loader?name=[name].[ext]!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

import configureStore from './configureStore';

// Import i18n messages
import { translationMessages } from './i18n';
import { EXPENSE_API, AMC_API, FC_PARENT_URL, FC_CLIENT_ID } from './config';
// Import CSS reset and Global Styles
import './global-styles';
import './global.less';

setGlobalHeader({
  headers: {
    'System-Provider': 'EXPENSE_MODULE',
  },
});
fiducialConnectAPIConfig.setParentURL(FC_PARENT_URL).setClientId(FC_CLIENT_ID);
expenseAPIConfig.setURL(EXPENSE_API);
amcAPIConfig.setURL(AMC_API);
// Create redux store with history
const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);
ErrorNotification.config({
  title: 'Oups.. something went wrong.',
  onReloadConfirm: () => {
    store.dispatch(resetApp());
  },
});
const MOUNT_NODE = document.getElementById('app');

const render = messages => {
  ReactDOM.render(
    <Provider store={store}>
      <LanguageProvider messages={messages}>
        <ConnectedRouter history={history}>
          <HashRouter>
            <Switch>
              <Route component={Auth} />
            </Switch>
          </HashRouter>
        </ConnectedRouter>
      </LanguageProvider>
    </Provider>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  new Promise(resolve => {
    resolve(import('intl'));
  })
    .then(() => Promise.all([import('intl/locale-data/jsonp/en.js')]))
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}
