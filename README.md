# Project launched from _**Fiducial React-Boilerplate**_ ([documentation](http://bitbucket.fiducial.com/projects/REACT/repos/boilerplate/browse/README.md))

## Installation

#### 1) Generate keys for bitbucket ([Instruction/click here](http://bitbucket.fiducial.com/projects/EXMD/repos/front-end/browse/keys/README.md))

#### 2) **Docker** and **docker-compose** needed

`docker-compose up` : to be executed at the **root of the project**, it will launch the container with our app in it 

## Help

### Setup my application on Fiducial Connect

The front-end address MUST add `/#/` at the end of the Application `Web Address`
http://
Example:
  https://app.fiducial.com/#/
