### Add SSH keys in order to be able to fetch AMC dependencies from the container
1) Generate **keys** into this folder ([Instruction here](https://confluence.atlassian.com/bitbucketserver049/creating-ssh-keys-852726368.html#CreatingSSHkeys-CreatinganSSHkeyonLinux&MacOSX))
2) Go to [this website](http://bitbucket.fiducial.com/plugins/servlet/ssh/account/keys) to **copy-paste** your public key (**id_rsa.pub**)
3) Be sure the two keys (id_rsa and id_rsa.pub) are in this folder **keys**
